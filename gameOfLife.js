(function () {

    if (typeof com === 'undefined') {
        com = {};
    }

    if (typeof com.mkiii === 'undefined') {
        com.mkiii = {};
    }

    com.mkiii.GameOfLife = function(sizeX, sizeY, density) {
        this.sizeX = sizeX;
        this.sizeY = sizeY;

        this.matrix = [];

        this.density = density;

        this.initRandom = function() {
            for (var i = 0; i < this.sizeX; i++) {
                this.matrix[i] = [];
                for (var j = 0; j < this.sizeY; j++) {
                    this.matrix[i][j] = Math.random()*100 >= (100-this.density) ? 1 : 0; 
                }
            }
        }

        this.proceedToNextGeneration = function() {
            var nextGeneration = [];
            for (var i = 0; i < this.matrix.length; i++) {
                nextGeneration[i] = [];
                for (var j = 0; j < this.matrix[i].length; j++) {
                    nextGeneration[i][j] = 0; 
                }
            }

            for (var i = 0; i < this.matrix.length; i++) {
                for (var j = 0; j < this.matrix[i].length; j++) {
                    var nrNeighbours = getNrNeighbours(this.matrix, i, j);
                    if (this.matrix[i][j] === 1 && (nrNeighbours === 2 || nrNeighbours === 3)) {
                        nextGeneration[i][j] = 1;
                    }
                    if (this.matrix[i][j] === 0 && nrNeighbours === 3) {
                        nextGeneration[i][j] = 1;
                    }
                }
            }

            for (var i = 0; i < this.matrix.length; i++) {
                this.matrix[i] = nextGeneration[i].slice();
            }
        }

        function getNrNeighbours(matrix, i, j) {
            var nrNeighbours = 0;
            for (var dx = -1; dx <= 1; dx++) {
                for (var dy = -1; dy <= 1; dy++) {
                    var x = i + dx;
                    var y = j + dy;
                    if (!(dx === 0 && dy === 0) && isWithinMatrix(matrix, x, y)) {
                        nrNeighbours += matrix[x][y];
                    }
                }
            }
            return nrNeighbours;
        }

        function isWithinMatrix(matrix, i, j) {
            return 0 <= i && i < matrix.length
             && 0 <= j && j < matrix[i].length;
        }
    }

})();