/*
Copyright 2018 Michel Klabbers

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR 
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
OTHER DEALINGS IN THE SOFTWARE.
*/

(function () {

    if (typeof com === 'undefined') {
        com = {};
    }

    if (typeof com.mkiii === 'undefined') {
        com.mkiii = {};
    }

    if (typeof com.mkiii.GridUtil === 'undefined') {
        throw Error("Need to load com.mkiii.GridUtil first");
    }

    var gridUtil = new com.mkiii.GridUtil();

    // TODOS/IDEAS: 
    // - map with components ordered by nr connections
    // - start with component with the most connections
    // - (first) identify groups of connected elements that are isolated from the rest
    // - get a better initial result

    com.mkiii.GridConnect = function(connections) {
        this.arrowSep = '->';
        this.uniqueElementKeys = getUniqueElementKeys(connections);
        this.connectMatrix = getConnectMatrix(connections);
        this.connectArray = getConnectArray(this.arrowSep, connections);
        this.paths = getPaths(this.connectMatrix);
        this.longestPathKey = getLongestPathKey(this.paths);
        this.longestPathLength = this.longestPathKey.split(",").length;
        this.gridSize = 2 * this.uniqueElementKeys.length + 2;
        this.grid = [];
        this.gridMap = {};
        this.gridStatistics = {};

        this.gridHistory = [];
        this.gridMapHistory = [];
        this.centerpointHistory = [];
        this.gridStateHistory = [];
        this.checksumHistory = []; 

        if (! Object.keys(this.paths) ||  Object.keys(this.paths).length === 0) {
            throw new Error('No paths found, cannot create grid');
        }
        
        console.log("=== Unique Elements ===");
        console.log(this.uniqueElementKeys);
        console.log("=== End Unique Elements ===");
        console.log(this.connectArray);
        console.log("=== Longest Path ===");
        console.log(this.longestPathKey);
        console.log("=== End Longest Path ===");

        for (var i = 0; i < this.gridSize; i++) {
            this.grid[i] = [];
            for (var j = 0; j < this.gridSize; j++) {
                this.grid[i][j] = {};
            }
        }
       
        var i = Math.floor(this.gridSize/2);
        var longestPathElements = this.longestPathKey.split(",");
        var previousElementKey = "";
        var nextElementKey = "";
        for (var j = 0; j < longestPathElements.length; j++) {

            if (j > 0) {
                previousElementKey = longestPathElements[j-1];
            } else {
                previousElementKey = "";
            }

            if (j < longestPathElements.length - 1) {
                nextElementKey = longestPathElements[j+1];
            } else {
                nextElementKey = "";
            }

            this.grid[i][this.gridSize/2-1+j+1] = {
                element: longestPathElements[j]
            }
            this.gridMap[longestPathElements[j]] = { 
                x: i, 
                y: this.gridSize/2-1 + j + 1,
                connectedFrom: [ previousElementKey ],
                connectedTo: [ nextElementKey ]
            };
        }

        updateHistory(this.gridHistory, this.gridMapHistory, this.grid, this.gridMap, this.centerpointHistory);

        var pathKeysContainingElements = Object.keys(this.paths);
        for (var i = 0; i < pathKeysContainingElements.length; i++) {
            var pathElements = pathKeysContainingElements[i].split(",");

            for (var j = 0; j < pathElements.length; j++) {

                var pathElement = pathElements[j];

                var previousElementKey = null;
                var nextElementKey = null;
                if (j > 0 && pathElements[j-1] && gridUtil.isElementKnownInGrid(this.gridMap, pathElements[j-1])) {
                    previousElementKey = pathElements[j-1];
                }
                if (j < pathElements.length - 1 && pathElements[j+1] && gridUtil.isElementKnownInGrid(this.gridMap, pathElements[j+1])) {
                    nextElementKey = pathElements[j+1];
                }

                if (gridUtil.isElementNotKnownYetInGrid(this.gridMap, pathElement)) {

                    var connectedPoints = [];
                    if (previousElementKey) {
                        connectedPoints.push(gridUtil.toPoint(this.gridMap[previousElementKey].x, this.gridMap[previousElementKey].y));
                    }
                    if (nextElementKey) {
                        connectedPoints.push(gridUtil.toPoint(this.gridMap[nextElementKey].x, this.gridMap[nextElementKey].y));
                    }

                    var bestGridPoint = gridUtil.getFreeGridPointWithShortestDistanceToPointsBalancedPreferred(this.grid, connectedPoints);

                    if (bestGridPoint.x > -1 && bestGridPoint.y > -1) {

                        assignElementToLocation(this.grid, this.gridMap, pathElement, bestGridPoint, previousElementKey ? [ previousElementKey ] : [], nextElementKey ? [ nextElementKey ] : []);

                    }
                    
                } else {
                    if (previousElementKey && this.gridMap[pathElement].connectedFrom.indexOf(previousElementKey) === -1) {
                        this.gridMap[pathElement].connectedFrom.push(previousElementKey);
                    }
                    if (nextElementKey && this.gridMap[pathElement].connectedTo.indexOf(nextElementKey) === -1) {
                        this.gridMap[pathElement].connectedTo.push(nextElementKey);
                    }
                } 
            }
        }

        calculateConnectionValuesAndSaveToGridStatistics(this.gridMap, this.gridStatistics);

        updateHistory(this.gridHistory, this.gridMapHistory, this.grid, this.gridMap, this.centerpointHistory);

        console.log("=== Relocating to Avoid Lines Through Elements ===");

        fixAllThroughElementsUntilAllHaveBeenFixed(this.grid, this.gridMap, this.gridStatistics);

        console.log("=== End Relocating to Avoid Lines Through Elements ===");

        updateHistory(this.gridHistory, this.gridMapHistory, this.grid, this.gridMap, this.centerpointHistory);

        removeEmptyElementsFromConnects(this.gridMap);

        gridUtil.clearConnectionPlaceholdersInGrid(this.grid);

        updateHistory(this.gridHistory, this.gridMapHistory, this.grid, this.gridMap, this.centerpointHistory);

        var isPreviousState = false;
        var maxNrRelocateRepeats = 5;
        var maxNrCenterRelocateRepeats = 5;
        var maxNrEndpointRelocateRepeats = 5;
        var nrRelocateRepeats = 0;
        var nrRelocationsDone = Number.MAX_SAFE_INTEGER;
        while (nrRelocationsDone > 0 && nrRelocateRepeats < maxNrRelocateRepeats && !isPreviousState) {
            nrRelocationsDone = 0;

            if (nrRelocateRepeats === 0) {
                var nrCenterRelocationsDone = Number.MAX_SAFE_INTEGER;
                var centerCounter = 0;
                while (centerCounter < maxNrCenterRelocateRepeats && nrCenterRelocationsDone > 0) {
                    var gridRelocateFunctionCloserToCenterPoint = new com.mkiii.GridRelocateFunctionCloserToCenterPoint(this.grid, this.gridMap);
                    gridRelocateFunctionCloserToCenterPoint.relocate(this.grid, this.gridMap, this.gridStatistics);
                    nrCenterRelocationsDone = gridRelocateFunctionCloserToCenterPoint.getNrRelocations();
                    centerCounter++;
                }
            }

            updateHistory(this.gridHistory, this.gridMapHistory, this.grid, this.gridMap, this.centerpointHistory);

            var gridRelocateFunctionCloserToAllOutgoingElements = new com.mkiii.GridRelocateFunctionCloserToAllOutgoingElements(this.grid, this.gridMap);
            gridRelocateFunctionCloserToAllOutgoingElements.relocate(this.grid, this.gridMap, this.gridStatistics);
            nrRelocationsDone += gridRelocateFunctionCloserToAllOutgoingElements.getNrRelocations();

            updateHistory(this.gridHistory, this.gridMapHistory, this.grid, this.gridMap, this.centerpointHistory);

            var gridRelocateFunctionCloserToAllIncomingElements = new com.mkiii.GridRelocateFunctionCloserToAllIncomingElements(this.grid, this.gridMap);
            gridRelocateFunctionCloserToAllIncomingElements.relocate(this.grid, this.gridMap, this.gridStatistics);
            nrRelocationsDone += gridRelocateFunctionCloserToAllIncomingElements.getNrRelocations();

            updateHistory(this.gridHistory, this.gridMapHistory, this.grid, this.gridMap, this.centerpointHistory);

            var gridRelocateFunctionCloserToNeighbours = new com.mkiii.GridRelocateFunctionCloserToNeighbours(this.grid, this.gridMap);
            gridRelocateFunctionCloserToNeighbours.relocate(this.grid, this.gridMap, this.gridStatistics);
            nrRelocationsDone += gridRelocateFunctionCloserToNeighbours.getNrRelocations();

            updateHistory(this.gridHistory, this.gridMapHistory, this.grid, this.gridMap, this.centerpointHistory);


            var endpointCounter = 0;
            var nrEndpointRelocationsDone = Number.MAX_SAFE_INTEGER;
            while (endpointCounter < maxNrEndpointRelocateRepeats && nrEndpointRelocationsDone > 0) {
                updateHistory(this.gridHistory, this.gridMapHistory, this.grid, this.gridMap, this.centerpointHistory);

                var gridRelocateFunctionCloserToOnlyIncomingElementNoOutgoingElements = new com.mkiii.GridRelocateFunctionCloserToOnlyIncomingElementNoOutgoingElements(this.grid, this.gridMap);
                gridRelocateFunctionCloserToOnlyIncomingElementNoOutgoingElements.relocate(this.grid, this.gridMap, this.gridStatistics);
                nrEndpointRelocationsDone = gridRelocateFunctionCloserToOnlyIncomingElementNoOutgoingElements.getNrRelocations();            

                updateHistory(this.gridHistory, this.gridMapHistory, this.grid, this.gridMap, this.centerpointHistory);

                var gridRelocateFunctionCloserToOnlyOutgoingElementNoIncomingElements = new com.mkiii.GridRelocateFunctionCloserToOnlyOutgoingElementNoIncomingElements(this.grid, this.gridMap);
                gridRelocateFunctionCloserToOnlyOutgoingElementNoIncomingElements.relocate(this.grid, this.gridMap, this.gridStatistics);
                nrEndpointRelocationsDone += gridRelocateFunctionCloserToOnlyOutgoingElementNoIncomingElements.getNrRelocations();

                endpointCounter++;
            }

            updateHistory(this.gridHistory, this.gridMapHistory, this.grid, this.gridMap, this.centerpointHistory);
    
            var repeatMinifiedGridResult = getMinifiedGrid(this.grid, this.gridMap, 2);
            this.grid = repeatMinifiedGridResult.minifiedGrid;
            this.gridMap = repeatMinifiedGridResult.minifiedGridMap;

            nrRelocateRepeats++;
            console.log("nr relocations done: " + nrRelocationsDone + ", nrRelocateRepeats: " + nrRelocateRepeats);

            updateHistory(this.gridHistory, this.gridMapHistory, this.grid, this.gridMap, this.centerpointHistory);

            var gridStateString = getGridStateString(this.grid);
            var checksum = gridUtil.getChecksum(gridStateString);
            if (this.checksumHistory.indexOf(checksum) !== -1 && this.gridStateHistory.indexOf(gridStateString) !== -1) {
                isPreviousState = true;
            }
            this.gridStateHistory.push(gridStateString);
            this.checksumHistory.push(checksum);
        }

        var minifiedGridResult = getMinifiedGrid(this.grid, this.gridMap);
        this.grid = minifiedGridResult.minifiedGrid;
        this.gridMap = minifiedGridResult.minifiedGridMap;

        updateHistory(this.gridHistory, this.gridMapHistory, this.grid, this.gridMap, this.centerpointHistory);

        function calculateConnectionValuesAndSaveToGridStatistics(gridMap, gridStatistics) {
            var totalConnections = 0;
            var gridMapKeys = Object.keys(gridMap);
            for (var i = 0; i < gridMapKeys.length; i++) {
                var elementKey = gridMapKeys[i];
                totalConnections += gridUtil.getNrConnections(gridMap[elementKey].connectedFrom);
                totalConnections += gridUtil.getNrConnections(gridMap[elementKey].connectedTo);
            }
            gridStatistics.totalElements = gridMapKeys.length;
            gridStatistics.totalConnections = totalConnections;
            gridStatistics.totalSize = gridStatistics.totalElements * gridStatistics.totalConnections;
            gridStatistics.averageConnections = 0;
            if (gridStatistics.totalElements > 0) {
                gridStatistics.averageConnections = gridStatistics.totalConnections / gridStatistics.totalElements;
            }
        }

        function getGridStateString(grid) {
            var gridStateString = "";
            for (var i = 0; i < grid.length; i++) {
                for (var j = 0; j < grid[i].length; j++) {
                    gridStateString += grid[i][j] + i + j;
                }
            }
            return gridStateString;
        }

        function updateHistory(gridHistory, gridMapHistory, grid, gridMap, centerpointHistory) {
            var gridCopy = [];
            for (var i = 0; i < grid.length; i++) {
                gridCopy[i] = [];
                for (var j = 0; j < grid[i].length; j++) {
                    gridCopy[i][j] = grid[i][j];
                }
            }
            gridHistory.push(gridCopy);

            var gridMapCopy = {};
            var gridMapKeys = Object.keys(gridMap);
            for (var i = 0; i < gridMapKeys.length; i++) {
                var elementKey = gridMapKeys[i];
                gridMapCopy[elementKey] = {
                    x: gridMap[elementKey].x, 
                    y: gridMap[elementKey].y,
                    connectedFrom: gridMap[elementKey].connectedFrom.slice(),
                    connectedTo: gridMap[elementKey].connectedTo.slice()            
                }
            }
            gridMapHistory.push(gridMapCopy);

            var centerpoint = gridUtil.getCenterPoint(gridMap);
            centerpointHistory.push({
                x: centerpoint.x,
                y: centerpoint.y
            })
        }

        function getMinifiedGrid(grid, gridMap, extraGridSize = 0) {
            console.log("=== Minify Grid ===");

            var jMinifiedGrid = [];
            var jMinifiediIndex = 0;
            for (var i = 0; i < grid.length; i++) {
                var lineIsEmpty = true;
                for (var j = 0; j < grid[i].length; j++) {
                    if (grid[i][j].element) {
                        lineIsEmpty = false;
                    }
                }
                if (!lineIsEmpty) {
                    jMinifiedGrid[jMinifiediIndex] = [];
                    for (var j = 0; j < grid[i].length; j++) {
                        jMinifiedGrid[jMinifiediIndex][j] = grid[i][j];
                    }
                    jMinifiediIndex++;
                }
            }
    
            console.log("jMinifiedGrid.length = " + jMinifiedGrid.length);
            console.log("jMinifiedGrid[0].length = " + jMinifiedGrid[0].length);
    
            var minifiedGrid = [];
            var iSize = jMinifiedGrid.length;
            var jSize = jMinifiedGrid[0].length;
            var minifiedjIndex = 0;
            for (var j = 0; j < jSize; j++) {
                var lineIsEmpty = true;
                for (var i = 0; i < iSize; i++) {
                    if (jMinifiedGrid[i][j].element) {
                        lineIsEmpty = false;
                    }
                }
                if (!lineIsEmpty) {
                    for (var i = 0; i < iSize; i++) {
                        if (!minifiedGrid[i]) {
                            minifiedGrid[i] = [];
                        }
                        minifiedGrid[i][minifiedjIndex] = jMinifiedGrid[i][j];
                    }
                    minifiedjIndex++;
                }
            }

            if (extraGridSize > 0) {
                var minifiedGridWithExtraGridSize = [];
                for (var i = 0; i < minifiedGrid.length + 2 * extraGridSize; i++) {
                    minifiedGridWithExtraGridSize[i] = [];
                    for (var j = 0; j < minifiedGrid[0].length + 2 * extraGridSize; j++) {
                        minifiedGridWithExtraGridSize[i][j] = {};
                    }
                }

                for (var i = 0; i < minifiedGrid.length; i++) {
                    for (var j = 0; j < minifiedGrid[i].length; j++) {
                        minifiedGridWithExtraGridSize[i + extraGridSize][j + extraGridSize] = minifiedGrid[i][j];
                    }
                }

                minifiedGrid = minifiedGridWithExtraGridSize;
            }
    
            console.log("minifiedGrid.length = " + minifiedGrid.length);
            console.log("minifiedGrid[0].length = " + minifiedGrid[0].length);
    
            var minifiedGridMap = {};
            for (var i = 0; i < minifiedGrid.length; i++) {
                for (var j = 0; j < minifiedGrid[i].length; j++) {
                    var elementAtThisLocation = minifiedGrid[i][j].element;
                    if (elementAtThisLocation) {
                        minifiedGridMap[elementAtThisLocation] = { 
                            x: i, 
                            y: j,
                            connectedFrom: gridMap[elementAtThisLocation].connectedFrom.slice(),
                            connectedTo: gridMap[elementAtThisLocation].connectedTo.slice()
                        };
                    }
                }
            }
    
            console.log("=== End Minify Grid ===");

            return {
                minifiedGrid: minifiedGrid,
                minifiedGridMap: minifiedGridMap
            };
        }

        function assignElementToLocation(grid, gridMap, elementKey, location, connectedFrom, connectedTo) {
            grid[location.x][location.y] = {
                element: elementKey
            }
            gridMap[elementKey] = { 
                x: location.x, 
                y: location.y,
                connectedFrom: connectedFrom,
                connectedTo: connectedTo
            };
        }

        function hasIncomingConnections(gridMap, gridMapKey) {
            return gridMap[gridMapKey].connectedFrom && gridMap[gridMapKey].connectedFrom.length > 0;
        }

        function hasNoOutgoingConnectionsOrOneIncomingConnection(gridMap, gridMapKey) {
            return (!gridMap[gridMapKey].connectedTo || gridMap[gridMapKey].connectedTo.length === 0)
            || (gridMap[gridMapKey].connectedFrom && gridMap[gridMapKey].connectedFrom.length === 1);
        }

        function removeEmptyElementsFromConnects(gridMap) {
            var gridMapKeys = Object.keys(gridMap);
            for (var i = 0; i < gridMapKeys.length; i++) {
                var gridMapKey = gridMapKeys[i];
                if (gridMap[gridMapKey].connectedFrom && gridMap[gridMapKey].connectedFrom.length > 0) {
                    if (gridMap[gridMapKey].connectedFrom.indexOf("") !== -1) {
                        gridMap[gridMapKey].connectedFrom = gridMap[gridMapKey].connectedFrom.filter(e => e !== "");
                    }
                }
            }
        }

        function fixAllThroughElementsUntilAllHaveBeenFixed(grid, gridMap, gridStatistics) {
            var allThroughElementsKeys = gridUtil.getElementsWhereConnectionLinesPassThrough(gridMap, grid, gridStatistics);
            console.log("Nr through elements, initial: " + allThroughElementsKeys.length);
            var nrFixes = 0;
            while (allThroughElementsKeys && allThroughElementsKeys.length > 0 && nrFixes < 100) {
                fixAllThroughElements(grid, gridMap, allThroughElementsKeys);
                allThroughElementsKeys = gridUtil.getElementsWhereConnectionLinesPassThrough(gridMap, grid, gridStatistics);
                console.log("Nr through elements: " + allThroughElementsKeys.length);
                nrFixes++;
            }
        }

        function fixAllThroughElements(grid, gridMap, throughElementsKeys) {
            for (var i = 0; i < throughElementsKeys.length; i++) {
                var throughElementKey = throughElementsKeys[i];
                var throughElement = gridMap[throughElementKey];

                var connectedElements = [];
                if (throughElement.connectedTo && throughElement.connectedTo.length > 0 && throughElement.connectedTo[0] !== "") {
                    connectedElements.push(throughElement.connectedTo);
                }

                if (throughElement.connectedFrom && throughElement.connectedFrom.length > 0 && throughElement.connectedFrom[0] !== "") {
                    connectedElements.push(throughElement.connectedFrom);
                }

                connectedElements = gridUtil.flatten(connectedElements);

                var connectedPoints = [];
                for (var j = 0; j < connectedElements.length; j++) {
                    addToConnectedPoints(connectedPoints, connectedElements[j]);
                }

                if (connectedPoints.length > 0) {
                    var bestGridPoint = gridUtil.getFreeGridPointWithShortestDistanceToPointsBalancedPreferred(grid, connectedPoints);

                    if (bestGridPoint.x > -1 && bestGridPoint.y > -1) {

                        grid[bestGridPoint.x][bestGridPoint.y] = {
                            element: throughElementKey
                        }
                        gridMap[throughElementKey] = { 
                            x: bestGridPoint.x, 
                            y: bestGridPoint.y,
                            connectedFrom: throughElement.connectedFrom,
                            connectedTo: throughElement.connectedTo
                        };

                        grid[throughElement.x][throughElement.y] = {
                            element: 'connection'
                        }
                    }
                }

                function addToConnectedPoints(connectedPoints, elementKey) {
                    connectedPoints.push({
                        x: gridMap[elementKey].x,
                        y: gridMap[elementKey].y
                    })
                }
            }
        }

        function getUniqueElementKeys(connections) {
            var uniqueElementKeys = [];
            for (var i = 0; i < connections.length; i++) {
                if (uniqueElementKeys.indexOf(connections[i].connectFrom) === -1) {
                    uniqueElementKeys.push(connections[i].connectFrom);
                }
                if (uniqueElementKeys.indexOf(connections[i].connectTo) === -1) {
                    uniqueElementKeys.push(connections[i].connectTo);
                }
            }
            return uniqueElementKeys;
        }

        function getConnectMatrix(connections) {
            var matrix = {};
            for (var i = 0; i < connections.length; i++) {
                if (!matrix[connections[i].connectFrom]) {
                    matrix[connections[i].connectFrom] = [];
                }
                matrix[connections[i].connectFrom][connections[i].connectTo] = 1;
            }
            return matrix;
        }

        function getConnectArray(arrowSep, connections) {
            var arr = {};
            for (var i = 0; i < connections.length; i++) {
                arr[connections[i].connectFrom + arrowSep + connections[i].connectTo] = {
                    connectTo: connections[i].connectTo,
                    connectType: connections[i].type
                }
            }
            return arr;
        }

        function getConnectedElementGroups(connectMatrix) {
            var allGroups = {};

            var connectFromNames = Object.keys(connectMatrix);

            for (var i = 0; i < connectFromNames.length; i++) {
                // TODO
            }
        }

        function getPaths(connectMatrix) {

            var allPaths = {};

            var connectFromNames = Object.keys(connectMatrix);

            for (var i = 0; i < connectFromNames.length; i++) {
                recursivePaths([ connectFromNames[i] ]);
            }

            var allPathsKeys = Object.keys(allPaths);
            var duplicateKeys = [];
            for (var i = 0; i < allPathsKeys.length; i++) {
                if (nrOccurrencesAsSubPathOrPathInPaths(allPathsKeys, allPathsKeys[i]) > 1) {
                    duplicateKeys.push(allPathsKeys[i]);
                }
            }

            for (var i = 0; i < duplicateKeys.length; i++) {
                delete allPaths[duplicateKeys[i]];
            }

            printPaths(allPaths);

            return allPaths;

            function recursivePaths(currentPath) {
                if (currentPath && currentPath.length > 0) {
                    var lastValue = currentPath[currentPath.length - 1];
                    if (connectMatrix[lastValue]) {
                        var connectToNames = Object.keys(connectMatrix[lastValue]);
                        for (var c = 0; c < connectToNames.length; c++) {
                            if (currentPath.indexOf(connectToNames[c]) === -1) {
                                var newPath = currentPath.slice();
                                newPath.push(connectToNames[c]);
                                recursivePaths(newPath);
                            }
                        }
                    } else {
                        var currentPathAsString = currentPath.join(",");
                        allPaths[currentPathAsString] = {
                            elementsOfPath: currentPath
                        };
                    }
                }
            }

            function nrOccurrencesAsSubPathOrPathInPaths(pathKeys, keyStringToCheck) {
                var count = 0;
                for (var i = 0; i < pathKeys.length; i++) {
                    if (pathKeys[i].indexOf(keyStringToCheck) !== -1) {
                        count++;
                    }
                }
                return count;
            }

        }

        function printPaths(pathsMap) {
            console.log("=== Paths ===");
            var allPathsKeys = Object.keys(pathsMap);
            for (var i = 0; i < allPathsKeys.length; i++) {
                console.log("pathsMap[" + allPathsKeys[i] + "]");
            }
            console.log("=== End Paths ===");
        }

        function getLongestPathKey(paths) {
            var longestPathString = "";
            var pathsKeys = Object.keys(paths);
            for (var i = 0; i < pathsKeys.length; i++) {
                if (pathsKeys[i].split(",").length > longestPathString.split(",").length) {
                    longestPathString = pathsKeys[i];
                }
            }
            return longestPathString;
        }

    }

    com.mkiii.GridConnect.prototype.getPaths = function() {
        return this.paths;
    }

    if (typeof module === 'undefined') {
        module = {};
    }

    if (typeof module.exports === 'undefined') {
        module.exports = {};
    }

    if (module && module.exports) {
        module.exports = com.mkiii.GridConnect;
    }

})();