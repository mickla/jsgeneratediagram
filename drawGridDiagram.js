/*
Copyright 2018 Michel Klabbers

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR 
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
OTHER DEALINGS IN THE SOFTWARE.
*/

(function () {

    if (typeof com === 'undefined') {
        com = {};
    }

    if (typeof com.mkiii === 'undefined') {
        com.mkiii = {};
    }

    if (typeof com.mkiii.GridUtil === 'undefined') {
        throw Error("Need to load com.mkiii.GridUtil first");
    }

    if (typeof com.mkiii.draw2D === 'undefined') {
        throw Error("Need to load com.mkiii.draw2D first");
    }

    if (typeof com.mkiii.GridConnect === 'undefined') {
        throw Error("Need to load com.mkiii.GridConnect first");
    }

    var gridUtil = new com.mkiii.GridUtil();

    com.mkiii.DrawGridDiagram = function (connections, sizeGridX = 120, sizeGridY = 100, canvasName = 'myCanvas', useDailyLocalStorage = false, elementNumbers) {
        this.connections = connections;
        this.sizeGridX = sizeGridX;
        this.sizeGridY = sizeGridY;

        this.colors = new com.mkiii.DrawDiagramColors(canvasName);

        this.gridConnect = {};
        if (typeof(Storage) !== "undefined" && useDailyLocalStorage) {
            var connectionsChecksum = gridUtil.getChecksum(JSON.stringify(connections));
            var today = new Date();
            var todayString = today.getDate() + "-" + today.getMonth() + "-" + today.getFullYear();
            var localStorageKey = gridUtil.getLocalStorageString(connectionsChecksum + "." + todayString);
            var gridConnectFromStorage = localStorage.getItem(localStorageKey);
            if (gridConnectFromStorage) {
                this.gridConnect = JSON.parse(gridConnectFromStorage);
            } else {
                this.gridConnect = new com.mkiii.GridConnect(this.connections);
                try {
                    localStorage.setItem(localStorageKey, JSON.stringify(this.gridConnect));
                } catch (error) {
                    console.log('warn: could not persist to local storage, clearing local storage now, then attempting persist again, msg: ' + error);
                    window.localStorage.clear();
                    localStorage.setItem(localStorageKey, JSON.stringify(this.gridConnect));
                }
            }
        } else {
            this.gridConnect = new com.mkiii.GridConnect(this.connections);
        }

        com.mkiii.draw2D.canvas = canvasName;

        var sizeRectX = this.sizeGridX * 0.28;
        var sizeRectY = this.sizeGridY * 0.28;

        var that = this;

        this.drawAnimated = function (animationDelayMillis = 50, primaryElements) {
            for (var historyIndex = 0; historyIndex < this.gridConnect.gridHistory.length; historyIndex++) {
                setTimeout(drawGridState, historyIndex * animationDelayMillis, that, historyIndex, primaryElements);
            }
        }

        this.draw = function(primaryElements) {
            drawGridState(this, this.gridConnect.gridHistory.length - 1, primaryElements);
        }

        function drawGridState(that, historyIndex, primaryElements) {

            if (historyIndex < 0 || historyIndex >= that.gridConnect.gridHistory.length) {
                return;
            }

            var grid = that.gridConnect.gridHistory[historyIndex];
            var gridMap = that.gridConnect.gridMapHistory[historyIndex];
            var centerpoint = that.gridConnect.centerpointHistory[historyIndex];

            var centerRect = getSmallRect(centerpoint.x, centerpoint.y);

            document.getElementById(canvasName).width = grid[0].length * that.sizeGridX;
            document.getElementById(canvasName).height = grid.length * that.sizeGridY;

            var elementType = [];
            var gridMapKeys = Object.keys(gridMap);
            for (var i = 0; i < gridMapKeys.length; i++) {
                var componentName = gridMapKeys[i];
                var componentProperties = gridMap[componentName];
                var rect = getRect(componentProperties.x, componentProperties.y);
                var froms = componentProperties.connectedFrom;
                var connectTypes = [];
                if (froms) {
                    for (var j = 0; j < froms.length; j++) {
                        if (froms[j]) {
                            var fillAndStrokeStyle = getConnectionStrokeAndFillStyle(componentName, froms[j]);
                            var otherComponent = gridMap[froms[j]];
                            var otherRect = getRect(otherComponent.x, otherComponent.y);
                            var connectArrayKey = froms[j] + that.gridConnect.arrowSep + componentName;
                            if (that.gridConnect.connectArray && that.gridConnect.connectArray[connectArrayKey].connectType && that.gridConnect.connectArray[connectArrayKey].connectType.length > 0) {
                                otherRect.connect(rect, that.gridConnect.connectArray[connectArrayKey].connectType, fillAndStrokeStyle, fillAndStrokeStyle);
                                connectTypes.push(that.gridConnect.connectArray[connectArrayKey].connectType);
                            } else {
                                otherRect.connect(rect, null, fillAndStrokeStyle, fillAndStrokeStyle);
                            }
                        }
                    }
                }

                elementType[i] = connectTypes.length > 0 && connectTypes[0].indexOf("jdbc") !== -1;
            }

            for (var i = 0; i < gridMapKeys.length; i++) {
                var componentName = gridMapKeys[i];
                var componentProperties = gridMap[componentName];
                var rect = getRect(componentProperties.x, componentProperties.y);
                var rectStrokeStyle = getRectStrokeStyle(componentName, primaryElements);

                if (elementType[i]) {
                    rect = getCup(componentProperties.x, componentProperties.y);
                    rect.fill('rgba(255, 255, 255, 0.55)');
                } else {
                    if (primaryElements && isNoPrimaryElement(componentName)) {
                        rect.fill(that.colors.getTransparentPastelBrownToPastelGreyGradient(rect.leftTopPoint, rect.rightBottomPoint));
                    } else {
                        rect.fill(that.colors.getPastelBrownToPastelGreyGradient(rect.leftTopPoint, rect.rightBottomPoint));
                    }
                }

                rect.draw(rectStrokeStyle);

                if (elementNumbers && (componentName in elementNumbers)) {
                    var isBigNumberGreaterThanZero = elementNumbers[componentName][0] > 0;

                    var sumNumbers = 0;
                    for (var numberIndex = 0; numberIndex < elementNumbers[componentName].length; numberIndex++) {
                        sumNumbers += elementNumbers[componentName][numberIndex];
                    }

                    if (isBigNumberGreaterThanZero) {
                        rect.fill(that.colors.getPastelOrangeToPastelRedGradient(rect.leftTopPoint, rect.rightBottomPoint));
                        rect.drawBigBackgroundText(elementNumbers[componentName][0], 'rgba(255, 0, 0, 0.66)');
                    } else if (sumNumbers > 0) {
                        rect.fill(that.colors.getPastelYellowToPastelOrangeGradient(rect.leftTopPoint, rect.rightBottomPoint));
                    } else {
                        rect.fill(that.colors.getPastelGreenToGreenGradient(rect.leftTopPoint, rect.rightBottomPoint));
                    }

                    for (var numberIndex = 0; numberIndex < elementNumbers[componentName].length; numberIndex++) {
                        var numberColors = ['rgba(44, 22, 255, 0.75)', 'rgba(255, 186, 157, 1)', 'rgba(128, 96, 255, 0.75)', 'rgba(255, 146, 117, 1)'];
                        var numberColor = isBigNumberGreaterThanZero ? 'rgba(100, 100, 100, 0.15)' : numberColors[numberIndex - 1];
                        if (numberIndex > 0 && numberIndex < 5) {
                            if (elementNumbers[componentName][numberIndex] > 0) {
                                rect.drawQuadrantBackgroundText(elementNumbers[componentName][numberIndex], numberIndex, numberColor);
                            }
                        }
                    }
                }
                rect.drawInnerText(componentName, rectStrokeStyle);
            }

            function isNoPrimaryElement(componentName) {
                return primaryElements.indexOf(componentName) === -1;
            }

            function getConnectionStrokeAndFillStyle(componentName, otherComponentName) {
                var connectionfillAndStrokeStyle = 'rgba(0, 0, 0, 1)';
                if (primaryElements && (isNoPrimaryElement(componentName) || isNoPrimaryElement(otherComponentName))) {
                    connectionfillAndStrokeStyle = 'rgba(0, 0, 0, 0.15)';
                }
                return connectionfillAndStrokeStyle;
            }

            function getRectStrokeStyle(componentName) {
                var rectStrokeStyle = 'rgba(0, 0, 0, 1)';
                if (primaryElements && isNoPrimaryElement(componentName)) {
                    rectStrokeStyle = 'rgba(0, 0, 0, 0.15)';
                }
                return rectStrokeStyle;
            }
        }

        function getGridConnectFromLocalStorage(connections) {
            var gridConnectFromLocalStorage = {};
            var localStorageKey = getLocalStorageKey(connections);
            var gridConnectJsonStringFromLocalStorage = localStorage.getItem(localStorageKey);
            if (gridConnectJsonStringFromLocalStorage) {
                gridConnectFromLocalStorage = JSON.parse(gridConnectJsonStringFromLocalStorage);
            }
            return gridConnectFromLocalStorage;
        }

        function addGridConnectToLocalStorage(connections, gridConnectToStore) {
            var localStorageKey = getLocalStorageKey(connections);
            try {
                localStorage.setItem(localStorageKey, JSON.stringify(gridConnectToStore));
            } catch (error) {
                console.log('warn: could not persist to local storage, clearing local storage now, then attempting persist again, msg: ' + error);
                window.localStorage.clear();
                localStorage.setItem(localStorageKey, JSON.stringify(gridConnectToStore));
            }
        }

        function getLocalStorageKey(connections) {
            var connectionsChecksum = gridUtil.getChecksum(JSON.stringify(connections));
            var today = new Date();
            var todayString = today.getDate() + "-" + today.getMonth() + "-" + today.getFullYear();
            return gridUtil.getLocalStorageString(connectionsChecksum + "." + todayString);
        }

        function getSmallRect(i, j) {
            var p1 = getP1(i, j);
            var p2 = getP1(i + 0.2, j + 0.2);
            var rect = com.mkiii.draw2D.rect(p1, p2);
            return rect;
        }

        function getRect(i, j) {
            var p1 = getP1(i, j);
            var p2 = getP2(i, j);
            var rect = com.mkiii.draw2D.rect(p1, p2);
            return rect;
        }

        function getCup(i, j) {
            var p1 = getP1(i, j);
            var p2 = getP2(i, j);
            var cup = com.mkiii.draw2D.cup(p1, p2);
            return cup;
        }

        function getP1(i, j) {
            return com.mkiii.draw2D.toPoint(sizeGridX * j + sizeRectX, sizeGridY * i + sizeRectY);
        }

        function getP2(i, j) {
            return com.mkiii.draw2D.toPoint(sizeGridX * j + 3 * sizeRectX, sizeGridY * i + 3 * sizeRectY);
        }
    }

})();