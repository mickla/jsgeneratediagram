var assert = require('assert');
var OrganizeNodesAndLinks = require('../organizeNodesAndLinks');
var forceDirectedTestData = require('./d3jsForceDirectedTestSet.json');

var oneSourceNoLinks = {
  "nodes": [
    {"id": "Myriel", "group": 1}
  ],
  "links": []
};

var twoSourcesOneLink = {
  "nodes": [
    {"id": "Myriel", "group": 1},
    {"id": "Napoleon", "group": 1}
  ],
  "links": [
    {"source": "Napoleon", "target": "Myriel", "value": 1}
  ]
};

var twoSourcesNoLinksTwoSets = {
  "nodes": [
    {"id": "Myriel", "group": 1},
    {"id": "Napoleon", "group": 1}
  ],
  "links": [
  ]
};

var fourSourcesTwoLinksTwoSets = {
  "nodes": [
    {"id": "Myriel", "group": 1},
    {"id": "Napoleon", "group": 1},
    {"id": "Mlle.Baptistine", "group": 2},
    {"id": "Mme.Magloire", "group": 2}
  ],
  "links": [
    {"source": "Napoleon", "target": "Myriel", "value": 1},
    {"source": "Mlle.Baptistine", "target": "Mme.Magloire", "value": 1}
  ]
};

var fiveSourcesFourLinksOneSet = {
  "nodes": [
    {"id": "Myriel", "group": 1},
    {"id": "Napoleon", "group": 1},
    {"id": "Mlle.Baptistine", "group": 1},
    {"id": "Mme.Magloire", "group": 1},
    {"id": "CountessdeLo", "group": 1}
  ],
  "links": [
    {"source": "Napoleon", "target": "Myriel", "value": 1},
    {"source": "Mlle.Baptistine", "target": "Myriel", "value": 1},
    {"source": "Mme.Magloire", "target": "Myriel", "value": 1},
    {"source": "CountessdeLo", "target": "Napoleon", "value": 1}
  ]
};

var nodeMatrixFiveSourcesFourLinksOneSetOptimal = [
  [ 'CountessdeLo', '' ],
  [ 'Napoleon', '' ],
  [ 'Myriel', 'Mme.Magloire' ],
  [ 'Mlle.Baptistine', '' ]
];

var nodeMatrixFiveSourcesFourLinksOneSetTwoCrossingLinks = [
  [ 'Mlle.Baptistine', '' ],
  [ 'Napoleon', '' ],
  [ 'Myriel', 'Mme.Magloire' ],
  [ 'CountessdeLo', '' ]
];

var linksCrossingNodesMatrixFiveSourcesFourLinksOneSetOptimal = [
  [ '', '' ],
  [ '', '' ],
  [ '', '' ],
  [ '', '' ]
];

var linksCrossingNodesMatrixFiveSourcesFourLinksOneSetTwoCrossingLinks = [
  [ '', '' ],
  [ 'X', '' ],
  [ 'X', '' ],
  [ '', '' ]
];

describe('OrganizeNodesAndLinks', function() {

  describe('one source no links', function() {
    var organizeNodesAndLinks = new OrganizeNodesAndLinks(oneSourceNoLinks.nodes, oneSourceNoLinks.links);
    it('check one source', function() {
      assert.equal(organizeNodesAndLinks.nodes.length, 1);
    });
    it('check no links', function() {
      assert.equal(organizeNodesAndLinks.links.length, 0);
    });
    it('one connected set is identified', function() {
      assert.equal(organizeNodesAndLinks.connectedSets.length, 1);
    });
    it('connected set holds the one node', function() {
      assert.equal(organizeNodesAndLinks.connectedSets[0][0].node, oneSourceNoLinks.nodes[0]);
    });
  });

  describe('two sources one link', function() {
    var organizeNodesAndLinks = new OrganizeNodesAndLinks(twoSourcesOneLink.nodes, twoSourcesOneLink.links);
    it('check two sources', function() {
      assert.equal(organizeNodesAndLinks.nodes.length, 2);
    });
    it('check one link', function() {
      assert.equal(organizeNodesAndLinks.links.length, 1);
    });
    it('one connected set is identified', function() {
      assert.equal(organizeNodesAndLinks.connectedSets.length, 1);
    });
    it('the connected set has length two', function() {
      assert.equal(organizeNodesAndLinks.connectedSets[0].length, 2);
    });
    it('connected set holds the two nodes', function() {
      assert.ok(arraysHaveSameElementsAndSameSize(organizeNodesAndLinks.connectedSets[0].map(t => t.node), twoSourcesOneLink.nodes));
    });
    it('connected set has the correct incoming links', function() {
      assert.equal(organizeNodesAndLinks.connectedSets[0].filter(t => t.node.id === "Myriel")[0].incomingLinks.length, 1);
      assert.equal(organizeNodesAndLinks.connectedSets[0].filter(t => t.node.id === "Napoleon")[0].incomingLinks.length, 0);
    });
    it('connected set has the correct outgoing links', function() {
      assert.equal(organizeNodesAndLinks.connectedSets[0].filter(t => t.node.id === "Myriel")[0].outgoingLinks.length, 0);
      assert.equal(organizeNodesAndLinks.connectedSets[0].filter(t => t.node.id === "Napoleon")[0].outgoingLinks.length, 1);
    });
  });

  describe('two sources no links', function() {
    var organizeNodesAndLinks = new OrganizeNodesAndLinks(twoSourcesNoLinksTwoSets.nodes, twoSourcesNoLinksTwoSets.links);
    it('check two sources', function() {
      assert.equal(organizeNodesAndLinks.nodes.length, 2);
    });
    it('check no links', function() {
      assert.equal(organizeNodesAndLinks.links.length, 0);
    });
    it('two connected sets are identified', function() {
      assert.equal(organizeNodesAndLinks.connectedSets.length, 2);
    });
    it('the connected sets have length one', function() {
      assert.equal(organizeNodesAndLinks.connectedSets[0].length, 1);
      assert.equal(organizeNodesAndLinks.connectedSets[1].length, 1);
    });
    it('connected sets hold the two nodes', function() {
      assert.equal(organizeNodesAndLinks.connectedSets[0][0].node, twoSourcesNoLinksTwoSets.nodes[0]);
      assert.equal(organizeNodesAndLinks.connectedSets[1][0].node, twoSourcesNoLinksTwoSets.nodes[1]);
    });
    it('connected sets have the correct incoming links', function() {
      assert.equal(organizeNodesAndLinks.connectedSets[0].filter(t => t.node.id === "Myriel")[0].incomingLinks.length, 0);
      assert.equal(organizeNodesAndLinks.connectedSets[1].filter(t => t.node.id === "Napoleon")[0].incomingLinks.length, 0);
    });
    it('connected sets have the correct outgoing links', function() {
      assert.equal(organizeNodesAndLinks.connectedSets[0].filter(t => t.node.id === "Myriel")[0].outgoingLinks.length, 0);
      assert.equal(organizeNodesAndLinks.connectedSets[1].filter(t => t.node.id === "Napoleon")[0].outgoingLinks.length, 0);
    });
  });

  describe('four sources two links', function() {
    var organizeNodesAndLinks = new OrganizeNodesAndLinks(fourSourcesTwoLinksTwoSets.nodes, fourSourcesTwoLinksTwoSets.links);
    it('check two sources', function() {
      assert.equal(organizeNodesAndLinks.nodes.length, 4);
    });
    it('check two links', function() {
      assert.equal(organizeNodesAndLinks.links.length, 2);
    });
    it('two connected sets are identified', function() {
      assert.equal(organizeNodesAndLinks.connectedSets.length, 2);
    });
    it('the connected sets have length two', function() {
      assert.equal(organizeNodesAndLinks.connectedSets[0].length, 2);
      assert.equal(organizeNodesAndLinks.connectedSets[1].length, 2);
    });
    it('connected sets hold the four nodes', function() {
      assert.ok(arraysHaveSameElementsAndSameSize(organizeNodesAndLinks.connectedSets[0].map(t => t.node), [ fourSourcesTwoLinksTwoSets.nodes[0], fourSourcesTwoLinksTwoSets.nodes[1] ]));
      assert.ok(arraysHaveSameElementsAndSameSize(organizeNodesAndLinks.connectedSets[1].map(t => t.node), [ fourSourcesTwoLinksTwoSets.nodes[2], fourSourcesTwoLinksTwoSets.nodes[3] ]));
    });
    it('connected sets have the correct incoming links', function() {
      assert.equal(organizeNodesAndLinks.connectedSets[0].filter(t => t.node.id === "Myriel")[0].incomingLinks.length, 1);
      assert.equal(organizeNodesAndLinks.connectedSets[0].filter(t => t.node.id === "Myriel")[0].incomingLinks[0], "Napoleon");
      assert.equal(organizeNodesAndLinks.connectedSets[0].filter(t => t.node.id === "Napoleon")[0].incomingLinks.length, 0);
      assert.equal(organizeNodesAndLinks.connectedSets[1].filter(t => t.node.id === "Mme.Magloire")[0].incomingLinks.length, 1);
      assert.equal(organizeNodesAndLinks.connectedSets[1].filter(t => t.node.id === "Mme.Magloire")[0].incomingLinks[0], "Mlle.Baptistine");
      assert.equal(organizeNodesAndLinks.connectedSets[1].filter(t => t.node.id === "Mlle.Baptistine")[0].incomingLinks.length, 0);
    });
    it('connected sets have the correct outgoing links', function() {
      assert.equal(organizeNodesAndLinks.connectedSets[0].filter(t => t.node.id === "Myriel")[0].outgoingLinks.length, 0);
      assert.equal(organizeNodesAndLinks.connectedSets[0].filter(t => t.node.id === "Napoleon")[0].outgoingLinks.length, 1);
      assert.equal(organizeNodesAndLinks.connectedSets[0].filter(t => t.node.id === "Napoleon")[0].outgoingLinks[0], "Myriel");
      assert.equal(organizeNodesAndLinks.connectedSets[1].filter(t => t.node.id === "Mlle.Baptistine")[0].outgoingLinks.length, 1);
      assert.equal(organizeNodesAndLinks.connectedSets[1].filter(t => t.node.id === "Mlle.Baptistine")[0].outgoingLinks[0], "Mme.Magloire");
      assert.equal(organizeNodesAndLinks.connectedSets[1].filter(t => t.node.id === "Mme.Magloire")[0].outgoingLinks.length, 0);
    });
  });

  describe('five sources four links', function() {
    var organizeNodesAndLinks = new OrganizeNodesAndLinks(fiveSourcesFourLinksOneSet.nodes, fiveSourcesFourLinksOneSet.links);
    it('connected set is sorted from the node with the most links to the node with the least number of links', function() {
      assert.equal(organizeNodesAndLinks.connectedSets.length, 1);
      assert.equal(organizeNodesAndLinks.connectedSets[0].length, 5);
      assert.equal(organizeNodesAndLinks.connectedSets[0][0].node.id, "Myriel");
      assert.equal(organizeNodesAndLinks.connectedSets[0][1].node.id, "Napoleon");
      assert.ok(organizeNodesAndLinks.connectedSets[0][0].hasIncomingLinks());
      assert.ok(organizeNodesAndLinks.connectedSets[0][1].hasIncomingLinks());
      assert.ok(organizeNodesAndLinks.connectedSets[0][1].hasOutgoingLinks());
   });
  });

  describe('debug matrix with fixed length string logging', function() {
    var organizeNodesAndLinks = new OrganizeNodesAndLinks(fiveSourcesFourLinksOneSet.nodes, fiveSourcesFourLinksOneSet.links);
    it('log', function() {
      var matrix = [
        ['Myriel', 'Napoleon', ''],
        ['', 'Mlle.Baptistine', ''],
        ['Mme.Magloire', '', '']];
      console.log(organizeNodesAndLinks.getDebugStringOfMatrix(matrix));
    });
  });

  describe('detect crossing links', function() {
    var organizeNodesAndLinks = new OrganizeNodesAndLinks(fiveSourcesFourLinksOneSet.nodes, fiveSourcesFourLinksOneSet.links);
    it('fiveSourcesFourLinksOneSetTwoCrossingLinks', function() {
      var nodeSet = organizeNodesAndLinks.connectedSets[0];
      var incomingLinksMap = organizeNodesAndLinks.incomingLinksMap;
      var nodeMatrix = nodeMatrixFiveSourcesFourLinksOneSetTwoCrossingLinks;
      assert.ok(isMatrixIdentical(linksCrossingNodesMatrixFiveSourcesFourLinksOneSetTwoCrossingLinks, organizeNodesAndLinks.getCrossingLinksPositionMatrix(nodeSet, incomingLinksMap, nodeMatrix)));
    });

    it('fiveSourcesFourLinksOneSetOptimal', function() {
      var nodeSet = organizeNodesAndLinks.connectedSets[0];
      var incomingLinksMap = organizeNodesAndLinks.incomingLinksMap;
      var nodeMatrix = nodeMatrixFiveSourcesFourLinksOneSetOptimal;
      assert.ok(isMatrixIdentical(linksCrossingNodesMatrixFiveSourcesFourLinksOneSetOptimal, organizeNodesAndLinks.getCrossingLinksPositionMatrix(nodeSet, incomingLinksMap, nodeMatrix)));
    });
  });

  function arraysHaveSameElementsAndSameSize(arrOne, arrTwo) {
    var holdSameElements = arrOne.length > 0 && arrOne.length === arrTwo.length;
    for (var i = 0; i < arrOne.length; i++) {
      var hasElement = false;
      for (var j = 0; j < arrTwo.length; j++) {
        if (arrOne[i] === arrTwo[j]) {
          hasElement = true;
        }
      }
      holdSameElements = holdSameElements && hasElement;
    }
    return holdSameElements;
  }

  function isMatrixIdentical(matrixOne, matrixTwo) {
    if (!matrixOne || !matrixTwo || matrixOne.length !== matrixTwo.length || matrixOne[0].length !== matrixTwo[0].length) {
      return false;
    }
    for (var i = 0; i < matrixOne.length; i++) {
      if (matrixOne[i].length !== matrixTwo[i].length) {
        return false;
      }
      for (var j = 0; j < matrixOne[i].length; j++) {
        if (matrixOne[i][j] !== matrixTwo[i][j]) {
          return false;
        }
      }
    }
    return true;
  }
});