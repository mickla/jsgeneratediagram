/*
Copyright 2018 Michel Klabbers

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR 
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
OTHER DEALINGS IN THE SOFTWARE.
*/

(function () {

    if (typeof com === 'undefined') {
        com = {};
    }

    if (typeof com.mkiii === 'undefined') {
        com.mkiii = {};
    }

    com.mkiii.draw2D = {
        canvas: 'myCanvas',
        initialFontSize: 128,
        fontSizeDimension: 'px',
        fontType: 'Arial',
        fontWeight: 'bold',
        arrowLineWidth: 2,
        arrowHeadWidth: 16,
        lineIntersectAccuracy: 8,
        minConnectLineLengthForText: 32,
        clearWhite: function() {
            var canvas = document.getElementById(this.canvas);
            var context = canvas.getContext('2d');
            var oldFillStyle = context.fillStyle;
            context.fillStyle = "rgba(255, 255, 255, 1)";
            context.fillRect(0, 0, canvas.width, canvas.height);
            context.fillStyle = oldFillStyle;
        },
        toPoint: function (x, y) {
            return { x: x, y: y };
        },
        toVector: function(dirX, dirY) {
            return {x: dirX, y: dirY };
        },
        distance: function(p1, p2) {
            return Math.sqrt( Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2) ); 
        },
        direction: function(p1, p2) {
            var directionX = p2.x - p1.x;
            var directionY = p2.y - p1.y;
            var maxDirectionAbsolute = Math.max(Math.abs(directionX), Math.abs(directionY));
            if (maxDirectionAbsolute > 0) {
                var directionX = directionX / maxDirectionAbsolute;
                var directionY = directionY / maxDirectionAbsolute;
            }
            return com.mkiii.draw2D.toVector(directionX, directionY);
        },
        translate: function(p, direction, multiplicationFactor = 1) {
            return {
                x: p.x + direction.x * multiplicationFactor,
                y: p.y + direction.y * multiplicationFactor
            };
        },
        translateBack: function(p, direction, multiplicationFactor = 1) {
            return {
                x: p.x - direction.x * multiplicationFactor,
                y: p.y - direction.y * multiplicationFactor
            };
        },
        invertDirection: function(vector) {
            return { x: vector.y, y: vector.x };
        },
        orthogonalDirection: function(vector) {
            var orthogonalX = 100.0;
            if (Math.abs(vector.x) > 0.01) {
                orthogonalX = -vector.y/vector.x;
            }
            var orthogonalXnormalizeFactor = 1;
            if (Math.abs(orthogonalX) > 0.01) {
                orthogonalXnormalizeFactor = Math.abs(orthogonalX);
            }
            var normalizeFactor = Math.max(1, orthogonalXnormalizeFactor);
            return { x: orthogonalX / normalizeFactor, y: 1.0 / normalizeFactor };
        },
        mirrorDirection: function(vector) {
            return { x: -vector.x, y: -vector.y };
        },
        get2DContext: function () {
            var canvas = document.getElementById(this.canvas);
            return canvas.getContext('2d');
        },
        getFontWithFontSize: function(fontSize) {
            return com.mkiii.draw2D.fontWeight + " " + fontSize + com.mkiii.draw2D.fontSizeDimension + " " + com.mkiii.draw2D.fontType;
        },
        lineIntersectsWithRectangles: function(rect, otherRect, startPointLine, endPointLine) {
            var dxInc = (endPointLine.x - startPointLine.x) / com.mkiii.draw2D.lineIntersectAccuracy;
            var dyInc = (endPointLine.y - startPointLine.y) / com.mkiii.draw2D.lineIntersectAccuracy;

            var intersect = false;
            for (var i = 1; i < com.mkiii.draw2D.lineIntersectAccuracy; i++) {
                var checkIntersectPoint = { x: startPointLine.x + i * dxInc, y: startPointLine.y + i * dyInc };
                if (rect.isPointInside(checkIntersectPoint) || otherRect.isPointInside(checkIntersectPoint)) {
                    intersect = true;
                }
            }
            return intersect;
        },
        rect: function (leftTopPoint, rightBottomPoint) {
            return {
                leftTopPoint: leftTopPoint,
                rightBottomPoint: rightBottomPoint,
                connectPointTop: com.mkiii.draw2D.toPoint((leftTopPoint.x + rightBottomPoint.x) / 2, leftTopPoint.y),
                connectPointLeft: com.mkiii.draw2D.toPoint(leftTopPoint.x, (leftTopPoint.y + rightBottomPoint.y) / 2),
                connectPointBottom: com.mkiii.draw2D.toPoint((leftTopPoint.x + rightBottomPoint.x) / 2, rightBottomPoint.y),
                connectPointRight: com.mkiii.draw2D.toPoint(rightBottomPoint.x, (leftTopPoint.y + rightBottomPoint.y) / 2),
                connectWidth: 6,
                dashWidth: 3,
                centerPoint: com.mkiii.draw2D.toPoint((leftTopPoint.x + rightBottomPoint.x) / 2, (leftTopPoint.y + rightBottomPoint.y) / 2),
                width: Math.abs(rightBottomPoint.x - leftTopPoint.x),
                height: Math.abs(rightBottomPoint.y - leftTopPoint.y),
                clearWhite: function() {
                    var context = com.mkiii.draw2D.get2DContext();
                    var oldFillStyle = context.fillStyle;
                    context.fillStyle = "rgba(255, 255, 255, 1)";
                    context.fillRect(this.leftTopPoint.x, this.leftTopPoint.y, this.rightBottomPoint.x - this.leftTopPoint.x, this.rightBottomPoint.y - this.leftTopPoint.y);
                    context.fillStyle = oldFillStyle;
                },
                fill: function(fillStyle = 'rgba(255, 255, 255, 1)') {
                    var context = com.mkiii.draw2D.get2DContext();
                    var oldFillStyle = context.fillStyle;
                    context.fillStyle = fillStyle;
                    context.fillRect(this.leftTopPoint.x, this.leftTopPoint.y, this.rightBottomPoint.x - this.leftTopPoint.x, this.rightBottomPoint.y - this.leftTopPoint.y);
                    context.fillStyle = oldFillStyle;
                },
                draw: function (strokeStyle = 'rgba(0, 0, 0, 1)') {
                    var context = com.mkiii.draw2D.get2DContext();
                    var oldStrokeStyle = context.strokeStyle;
                    context.strokeStyle = strokeStyle;
                    context.beginPath();
                    context.moveTo(this.leftTopPoint.x, this.leftTopPoint.y);
                    context.lineTo(this.leftTopPoint.x, this.rightBottomPoint.y);
                    context.lineTo(this.rightBottomPoint.x, this.rightBottomPoint.y);
                    context.lineTo(this.rightBottomPoint.x, this.leftTopPoint.y);
                    context.lineTo(this.leftTopPoint.x, this.leftTopPoint.y);
                    context.stroke();
                    context.strokeStyle = oldStrokeStyle;
                },
                drawConnections: function () {
                    drawConnection(this.connectPointTop, this.connectWidth, this.dashWidth);
                    drawConnection(this.connectPointLeft, this.connectWidth, this.dashWidth);
                    drawConnection(this.connectPointBottom, this.connectWidth, this.dashWidth);
                    drawConnection(this.connectPointRight, this.connectWidth, this.dashWidth);

                    function drawConnection(point, connectWidth, dashWidth) {
                        var context = com.mkiii.draw2D.get2DContext();
                        context.beginPath();
                        context.setLineDash([dashWidth, dashWidth]);
                        context.moveTo(point.x - connectWidth, point.y - connectWidth);
                        context.lineTo(point.x + connectWidth, point.y + connectWidth);
                        context.stroke();
                        context.moveTo(point.x - connectWidth, point.y + connectWidth);
                        context.lineTo(point.x + connectWidth, point.y - connectWidth);
                        context.stroke();
                        context.setLineDash([]);
                    }
                },
                drawQuadrantBackgroundText: function(text, quadrant, fillStyle = "rgba(96, 96, 96, 0.25)") {
                    var centerX;
                    var bottomY;
                    var repositionTopX = 0.14 * Math.abs(this.rightBottomPoint.x - this.leftTopPoint.x);
                    var repositionTopY = 0.14 * Math.abs(this.rightBottomPoint.y - this.leftTopPoint.y);
                    var repositionBottomX = 0.14 * Math.abs(this.rightBottomPoint.x - this.leftTopPoint.x);
                    var repositionBottomY = 0.02 * Math.abs(this.rightBottomPoint.y - this.leftTopPoint.y);
                    if (quadrant === 1 || quadrant === "1") {
                        centerX = (this.leftTopPoint.x + this.centerPoint.x) / 2 - repositionTopX;
                        bottomY = this.centerPoint.y - repositionTopY;
                    }
                    if (quadrant === 2 || quadrant === "2") {
                        centerX = (this.centerPoint.x + this.rightBottomPoint.x) / 2 - repositionTopX;
                        bottomY = this.centerPoint.y - repositionTopY;
                    }
                    if (quadrant === 3 || quadrant === "3") {
                        centerX = (this.leftTopPoint.x + this.centerPoint.x) / 2 - repositionTopX;
                        bottomY = this.rightBottomPoint.y - repositionBottomY;
                    }
                    if (quadrant === 4 || quadrant === "4") {
                        centerX = (this.centerPoint.x + this.rightBottomPoint.x) / 2 - repositionTopX;
                        bottomY = this.rightBottomPoint.y - repositionBottomY;
                    }
                    if (centerX && bottomY) {
                        var context = com.mkiii.draw2D.get2DContext();
                        var oldFont = context.font;
                        context.font = com.mkiii.draw2D.getFontWithFontSize(this.height/3);
                        context.fillStyle = fillStyle;
                        var x = centerX - context.measureText(text).width/4;
                        var y = bottomY;
                        context.fillText(text, x, y);
                        context.font = oldFont;
                    }
                },
                drawBigBackgroundText: function(text, fillStyle = "rgba(255, 0, 0, 0.5)") {
                    var context = com.mkiii.draw2D.get2DContext();
                    var oldFont = context.font;
                    context.font = com.mkiii.draw2D.getFontWithFontSize(this.height * 1.2);
                    context.fillStyle = fillStyle;
                    var x = this.centerPoint.x - context.measureText(text).width/2;
                    var y = this.rightBottomPoint.y - 0.05 * Math.abs(this.rightBottomPoint.y - this.leftTopPoint.y);
                    context.fillText(text, x, y);
                    context.font = oldFont;
                },
                drawInnerText: function (text, fillStyle = "rgba(0, 0, 0, 1)") {
                    if (!text) {
                        return;
                    }

                    try {
                        var fontSizeTextFitsInRect = getFontSizeTextFitsInRect(text, com.mkiii.draw2D.initialFontSize, this.width, this.height);

                        if (fontSizeTextFitsInRect.fontSizeFit > 0) {
                            var context = com.mkiii.draw2D.get2DContext();
                            context.font = com.mkiii.draw2D.getFontWithFontSize(fontSizeTextFitsInRect.fontSizeFit);
                            for (var i = 0; i < fontSizeTextFitsInRect.wordsPerLine.length; i++) {
                                var lineText = fontSizeTextFitsInRect.wordsPerLine[i].join(" ");
                                var lineTextWidth = context.measureText(lineText).width;
                                var x = this.centerPoint.x - lineTextWidth/2;
                                var y = this.centerPoint.y - (fontSizeTextFitsInRect.wordsPerLine.length/2 - (i+1)) * (fontSizeTextFitsInRect.fontSizeFit);
                                context.fillStyle = fillStyle;
                                context.fillText(lineText, x, y);
                            }
                        } else {
                            console.log('Warning: couldn\'t draw text \'' + text + '\' in rect, text too large for rect');
                        }

                    } catch (error) {
                        console.log(error);
                    }

                    function getFontSizeTextFitsInRect(text, fontSize, width, height) {
                        var nrChars = text.length;
                        var words = getSplitText(text);
                        var fontSizeFit = 0;
                        var fontSizeTryFit = fontSize;
                        var isFit = false;
                        var wordsPerLine = [];
                        while (fontSizeTryFit > 2 && !isFit) {
                            var context = com.mkiii.draw2D.get2DContext();
                            context.font = com.mkiii.draw2D.getFontWithFontSize(fontSizeTryFit);

                            var maxLines = height / fontSizeTryFit - 1;
                            if (maxLines >= 1) {
                                var nrLines = 1;
                                while (nrLines < maxLines && !isFit) {
                                    var fitTextInLines = getFitTextInLines(nrLines, words, fontSizeTryFit, width);
                                    isFit = isFit || fitTextInLines.isFitTextInLines;
                                    if (isFit) {
                                        wordsPerLine = fitTextInLines.wordsPerLine;
                                    }
                                    nrLines++;
                                }

                                if (isFit) {
                                    fontSizeFit = fontSizeTryFit;
                                }
                            }

                            fontSizeTryFit = fontSizeTryFit - 2;
                        }

                        if (!isFit) {
                            throw Error('Text \'' + text + '\' does not fit');
                        }

                        return { fontSizeFit: fontSizeFit, wordsPerLine: wordsPerLine };

                        function getSplitText(text) {
                            var textToSplit = text;
                            textToSplit = textToSplit.replace("-", " ").replace("_", " ");

                            var firstUpperCaseFromWordIndices = [];
                            for (var i = 1; i < textToSplit.length - 1; i++) {
                                if (alphanumeric(textToSplit.charAt(i)) && isUppercase(textToSplit, i) && !isUppercase(textToSplit, i+1)) {
                                    firstUpperCaseFromWordIndices.push(i-1);
                                }
                            }

                            for (var i = 0; i < firstUpperCaseFromWordIndices.length; i++) {
                                var addSpaceIndex = firstUpperCaseFromWordIndices[i] + i + 1;
                                textToSplit = textToSplit.substring(0, addSpaceIndex) + " " + textToSplit.substring(addSpaceIndex);
                            }

                            var splitText = textToSplit.split(" ");

                            return splitText;

                            function isUppercase(text, index) {
                                return text.charAt(index) === text.charAt(index).toUpperCase();
                            }

                            function alphanumeric(text) {
                                var letterNumber = /^[0-9a-zA-Z]+$/;
                                return text.match(letterNumber);
                            }
                        }

                        function getFitTextInLines(nrLines, words, fontSizeTryFit, width) {
                            var lineNr = 1;
                            var wordsIndexStart = 0;
                            var isFitTextInLine = true;
                            var isFitTextInLines = false;
                            var wordsPerLine = [];
                            while (lineNr <= nrLines && isFitTextInLine && !isFitTextInLines) {
                                var fitTextInLine = getFitTextInLine(words, wordsIndexStart, fontSizeTryFit, width);
                                wordsIndexStart = fitTextInLine.wordsIndexStart;
                                isFitTextInLine = fitTextInLine.isFitTextInLine;
                                isFitTextInLines = fitTextInLine.allWordsFit;
                                wordsPerLine.push(fitTextInLine.wordsThatFit)
                                lineNr++;
                            }

                            return { isFitTextInLines: isFitTextInLines, wordsPerLine: wordsPerLine };

                            function getFitTextInLine(words, wordsIndexStart, fontSizeTryFit, width) {
                                var wordsIndexEnd = words.length;
                                var lineFit = false;
                                var allWordsFit = false;
                                var wordsThatFit = [];
                                while (wordsIndexEnd >= 1 && !lineFit) {
                                    lineFit = lineFit || isFitWordsInLine(words, wordsIndexStart, wordsIndexEnd, width);
                                    if (lineFit) {
                                        if (wordsIndexEnd === words.length) {
                                            allWordsFit = true;
                                        }
                                        break;
                                    }
                                    wordsIndexEnd--;
                                }

                                if (wordsIndexEnd < 1) {
                                    return { isFitTextInLine: false, wordsIndexStart: wordsIndexStart, allWordsFit: false, wordsThatFit: [] };
                                }

                                for (var i = wordsIndexStart; i < wordsIndexEnd; i++) {
                                    wordsThatFit.push(words[i]);
                                }
                                wordsIndexStart = wordsIndexEnd;

                                return { isFitTextInLine: true, wordsIndexStart: wordsIndexStart, allWordsFit: allWordsFit, wordsThatFit: wordsThatFit };

                                function isFitWordsInLine(words, wordsIndexStart, wordsIndexEnd, width) {
                                    var context = com.mkiii.draw2D.get2DContext();
                                    var textString = "";
                                    for (var i = wordsIndexStart; i < wordsIndexEnd; i++) {
                                        textString += words[i] + " ";
                                    }
                                    var textWidth = context.measureText(textString).width;

                                    return textWidth < width;
                                }
                            }
                        }
                    }
                },
                connect: function(otherRect, connectText, strokeStyle, fillStyle) {
                    var context = com.mkiii.draw2D.get2DContext();
                    var oldStrokeStyle = context.strokeStyle;
                    var oldFillStyle = context.fillStyle;
                    if (strokeStyle) {
                        context.strokeStyle = strokeStyle;
                    }
                    if (fillStyle) {
                        context.fillStyle = fillStyle;
                    }

                    var arrowLineWidth = Math.min(this.width, this.height)/12;
                    var arrowHeadWidth = Math.min(this.width, this.height)/11;
                    var connectPoints = getClosestConnectPoints(this, otherRect);
                    if (connectPoints && !com.mkiii.draw2D.lineIntersectsWithRectangles(this, otherRect, connectPoints.p, connectPoints.q)) {
                        com.mkiii.draw2D.arrow(connectPoints.p, connectPoints.q, arrowLineWidth, arrowHeadWidth).draw();
                    } else {
                        connectPoints = getConnectPointsOnCenterDistance(this, otherRect);
                        if (connectPoints && !com.mkiii.draw2D.lineIntersectsWithRectangles(this, otherRect, connectPoints.p, connectPoints.q)) {
                            com.mkiii.draw2D.arrow(connectPoints.p, connectPoints.q, arrowLineWidth, arrowHeadWidth).draw();
                        }
                    }

                    if (connectText) {
                        var connectionAveragePoint = { 
                            x: (connectPoints.p.x + connectPoints.q.x) / 2,
                            y: (connectPoints.p.y + connectPoints.q.y) / 2
                        };
                        var connectLineLength = com.mkiii.draw2D.distance(connectPoints.p, connectPoints.q);
                        var connectLineHeight = Math.abs(connectPoints.q.y - connectPoints.p.y);
                        var connectLineWidth = Math.abs(connectPoints.q.x - connectPoints.p.x);
                        var textRectWidth = this.width/2; //connectLineWidth/3 > this.width/2 ? connectLineWidth/3 : this.width/2;
                        var textRectHeight =  this.height/2; //connectLineHeight/4 > this.height/2 ? connectLineHeight/4 : this.height/2;
                        var direction = com.mkiii.draw2D.direction(connectPoints.p, connectPoints.q);
                        var orthogonalDirection = com.mkiii.draw2D.orthogonalDirection(direction);
                        if (direction.x < 0) {
                            orthogonalDirection = com.mkiii.draw2D.mirrorDirection(orthogonalDirection);
                        }
                        var obliqueness = Math.abs(direction.x) + Math.abs(direction.y) - 1;
                        var orthogonalOnlyX = Math.abs(orthogonalDirection.y) < 0.015 ? 1 : 0;
                        var orthogonalNegativeY = orthogonalDirection.y < 0 ? 1 : 0;
                        var rectMinSize = Math.min(this.width, this.height);
                        var textAveragePoint = com.mkiii.draw2D.translate(connectionAveragePoint, orthogonalDirection, (rectMinSize/10) * (1 + obliqueness/2 + 2 * orthogonalOnlyX + orthogonalNegativeY));
                        var nrCharsAfterFour = Math.max(0, connectText.length - 4);
                        var translateCharsAfterFourLeft = nrCharsAfterFour * 5;//orthogonalDirection.x < 0 ? nrCharsAfterFour * 6 : 0;
                        var translateCharsAfterFourRight = nrCharsAfterFour * 5;//orthogonalDirection.x > 0 ? nrCharsAfterFour * 6 : 0;

                        var textStartPoint = {
                            x: textAveragePoint.x - textRectWidth/2 - translateCharsAfterFourLeft,// - 100,
                            y: textAveragePoint.y - textRectHeight/2// - 100
                        };
                        var textEndPoint = {
                            x: textAveragePoint.x + textRectWidth/2 + translateCharsAfterFourRight,
                            y: textAveragePoint.y + textRectHeight/2
                        };

                        if (connectLineLength > com.mkiii.draw2D.minConnectLineLengthForText) {
                            var textRect = com.mkiii.draw2D.rect(textStartPoint, textEndPoint);
                            //textRect.clearWhite();
                            //textRect.draw('rgba(127, 127, 127, 1)');
                            textRect.drawInnerText(connectText, strokeStyle);// + ":" + obliqueness + " " + direction.x.toFixed(2) + "," + direction.y.toFixed(2) + " " + orthogonalDirection.x.toFixed(2) + "," + orthogonalDirection.y.toFixed(2));
                        }

                    }

                    context.strokeStyle = oldStrokeStyle;
                    context.fillStyle = oldFillStyle;

                    function getClosestConnectPoints(rect, otherRect) {
                        var connectionsThis = [rect.connectPointBottom, rect.connectPointLeft, rect.connectPointRight, rect.connectPointTop];
                        var connectionsOther = [otherRect.connectPointBottom, otherRect.connectPointLeft, otherRect.connectPointRight, otherRect.connectPointTop];
    
                        var closestConnectionThis;
                        var closestConnectionOther;
                        var minDist = Number.MAX_SAFE_INTEGER;
                        for (var i = 0; i < connectionsThis.length; i++) {
                            for (var j = 0; j < connectionsOther.length; j++) {
                                var dist = com.mkiii.draw2D.distance(connectionsThis[i], connectionsOther[j]);
                                if (dist < minDist) {
                                    closestConnectionThis = connectionsThis[i];
                                    closestConnectionOther = connectionsOther[j];
                                    minDist = dist;
                                }
                            }
                        }
    
                        if (closestConnectionThis && closestConnectionOther) {
                            return { p: closestConnectionThis, q: closestConnectionOther };
                        }
                        
                        return {};
                    }

                    function getConnectPointsOnCenterDistance(rect, otherRect) {
                        var centerDistX = rect.centerPoint.x - otherRect.centerPoint.x;
                        var centerDistY = rect.centerPoint.y - otherRect.centerPoint.y;
    
                        if (Math.abs(centerDistX) < Math.abs(centerDistY)) {
                            if (centerDistX < 0) {
                                return { p: rect.connectPointRight, q: otherRect.connectPointLeft };
                            } else {
                                return { p: rect.connectPointLeft, q: otherRect.connectPointRight };
                            }
                        } else {
                            if (centerDistY < 0) {
                                return { p: rect.connectPointBottom, q: otherRect.connectPointTop };
                            } else {
                                return { p: rect.connectPointTop, q: otherRect.connectPointBottom };
                            }
                        }
                        return {};
                    }
                },
                isPointInside(point) {
                    return this.leftTopPoint.x <= point.x && point.x <= this.rightBottomPoint.x
                        && this.leftTopPoint.y <= point.y && point.y <= this.rightBottomPoint.y;
                }
            };
        },
        cloud: function (leftTopPoint, rightBottomPoint) {
            var cloudRect = com.mkiii.draw2D.rect(leftTopPoint, rightBottomPoint);
            cloudRect.cloudHeight = cloudRect.height/8;
            cloudRect.draw = function () {
                    var context = com.mkiii.draw2D.get2DContext();
                    context.beginPath();
                    drawBezier(context, this.leftTopPoint.x, this.rightBottomPoint.y, this.connectPointBottom.x, this.connectPointBottom.y, 0, this.cloudHeight);
                    drawBezier(context, this.connectPointBottom.x, this.connectPointBottom.y, this.rightBottomPoint.x, this.rightBottomPoint.y, 0, this.cloudHeight);

                    drawBezier(context, this.rightBottomPoint.x, this.rightBottomPoint.y, this.connectPointRight.x, this.connectPointRight.y, this.cloudHeight, 0);
                    drawBezier(context, this.connectPointRight.x, this.connectPointRight.y, this.rightBottomPoint.x, this.leftTopPoint.y, this.cloudHeight, 0);

                    drawBezier(context, this.rightBottomPoint.x, this.leftTopPoint.y, this.connectPointTop.x, this.connectPointTop.y, 0, -this.cloudHeight);
                    drawBezier(context, this.connectPointTop.x, this.connectPointTop.y, this.leftTopPoint.x, this.leftTopPoint.y, 0, -this.cloudHeight);

                    drawBezier(context, this.leftTopPoint.x, this.leftTopPoint.y, this.connectPointLeft.x, this.connectPointLeft.y, -this.cloudHeight, 0);
                    drawBezier(context, this.connectPointLeft.x, this.connectPointLeft.y, this.leftTopPoint.x, this.rightBottomPoint.y, -this.cloudHeight, 0);

                    context.stroke();

                    function drawBezier(context, px, py, qx, qy, dx, dy) {
                        context.moveTo(px, py);
                        context.bezierCurveTo(px + dx, py + dy, qx + dx, qy + dy, qx, qy);
                    }
            }
            return cloudRect;
        },
        cup: function (leftTopPoint, rightBottomPoint) {
            var cupRect = com.mkiii.draw2D.rect(leftTopPoint, rightBottomPoint);
            cupRect.cupHeight = cupRect.height/8;
            cupRect.draw = function (strokeStyle = 'rgba(0, 0, 0, 1)') {
                var context = com.mkiii.draw2D.get2DContext();
                context.beginPath();

                var oldStrokeStyle = context.strokeStyle;
                context.strokeStyle = strokeStyle;

                drawBezier(context, this.leftTopPoint.x, this.leftTopPoint.y, this.rightBottomPoint.x, this.leftTopPoint.y, 0, -this.cupHeight);

                context.moveTo(this.leftTopPoint.x, this.leftTopPoint.y);
                context.lineTo(this.leftTopPoint.x, this.rightBottomPoint.y);
                context.moveTo(this.rightBottomPoint.x, this.leftTopPoint.y);
                context.lineTo(this.rightBottomPoint.x, this.rightBottomPoint.y);

                drawBezier(context, this.leftTopPoint.x, this.rightBottomPoint.y, this.rightBottomPoint.x, this.rightBottomPoint.y, 0, this.cupHeight);

                drawBezier(context, this.leftTopPoint.x, this.leftTopPoint.y, this.rightBottomPoint.x, this.leftTopPoint.y, 0, this.cupHeight);
                drawBezier(context, this.leftTopPoint.x, this.rightBottomPoint.y, this.rightBottomPoint.x, this.rightBottomPoint.y, 0, -this.cupHeight);
                context.stroke();

                context.strokeStyle = oldStrokeStyle;

            }

            cupRect.fill = function(fillStyle = 'rgba(255, 255, 255, 1)') {
                var context = com.mkiii.draw2D.get2DContext();
                context.beginPath();

                var oldFillStyle = context.fillStyle;
                context.fillStyle = fillStyle;

                context.moveTo(this.leftTopPoint.x, this.leftTopPoint.y);
                context.lineTo(this.leftTopPoint.x, this.rightBottomPoint.y);
                context.moveTo(this.rightBottomPoint.x, this.leftTopPoint.y);
                context.lineTo(this.rightBottomPoint.x, this.rightBottomPoint.y);

                drawBezier(context, this.leftTopPoint.x, this.leftTopPoint.y, this.rightBottomPoint.x, this.leftTopPoint.y, 0, -this.cupHeight);
                drawBezier(context, this.leftTopPoint.x, this.rightBottomPoint.y, this.rightBottomPoint.x, this.rightBottomPoint.y, 0, this.cupHeight);

                context.fill();
                context.fillRect(this.leftTopPoint.x, this.leftTopPoint.y, cupRect.width, cupRect.height);

                context.fillStyle = oldFillStyle;
            }

            function drawBezier(context, px, py, qx, qy, dx, dy) {
                context.moveTo(px, py);
                context.bezierCurveTo(px + dx, py + dy, qx + dx, qy + dy, qx, qy);
            }

            return cupRect;
        },
        arrow: function(startPoint, endPoint, lineWidth = this.arrowLineWidth, headWidth = this.arrowHeadWidth) {
            return {
                startPoint: startPoint,
                endPoint: endPoint,
                lineWidth: lineWidth,
                headWidth: headWidth,
                direction: com.mkiii.draw2D.direction(startPoint, endPoint),
                draw: function() {
                    var orthogonalDirection = com.mkiii.draw2D.invertDirection(this.direction);
                    var arrowHeadOneTranslateX = this.headWidth * this.direction.x - 0.5 * this.headWidth * orthogonalDirection.x;
                    var arrowHeadOneTranslateY = this.headWidth * this.direction.y + 0.5 * this.headWidth * orthogonalDirection.y;

                    var arrowHeadTwoTranslateX = this.headWidth * this.direction.x + 0.5 * this.headWidth * orthogonalDirection.x;
                    var arrowHeadTwoTranslateY = this.headWidth * this.direction.y - 0.5 * this.headWidth * orthogonalDirection.y;

                    var context = com.mkiii.draw2D.get2DContext();
                    context.beginPath();
                    context.moveTo(this.endPoint.x, this.endPoint.y);
                    context.lineTo(this.endPoint.x - arrowHeadOneTranslateX, this.endPoint.y - arrowHeadOneTranslateY);
                    context.lineTo(this.endPoint.x - arrowHeadTwoTranslateX, this.endPoint.y - arrowHeadTwoTranslateY);
                    context.fill();
                    context.beginPath();
                    context.moveTo(this.startPoint.x, this.startPoint.y);
                    context.lineTo(this.endPoint.x, this.endPoint.y);
                    context.stroke();
                }
            }
        }
    }

})();