/*
Copyright 2018 Michel Klabbers

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR 
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
OTHER DEALINGS IN THE SOFTWARE.
*/

(function () {

    if (typeof com === 'undefined') {
        com = {};
    }

    if (typeof com.mkiii === 'undefined') {
        com.mkiii = {};
    }

    if (typeof com.mkiii.GridUtil === 'undefined') {
        throw Error("Need to load com.mkiii.GridUtil first");
    }

    var gridUtil = new com.mkiii.GridUtil();

    com.mkiii.GridRelocateFunction = function (grid, gridMap, relocateFunction) {
        this.nrRelocations = 0;

        if (!relocateFunction.getPointToBeConsideredParameters) {
            relocateFunction.getPointToBeConsideredParameters = function() { return {}; }
        }

        this.relocate = function (grid, gridMap, gridStatistics) {
            console.log("=== Attempting To " + relocateFunction.getDescription() + " ===");
            this.nrRelocations = 0;
            var gridMapKeys = Object.keys(gridMap);
            for (var i = 0; i < gridMapKeys.length; i++) {

                var gridMapKey = gridMapKeys[i];

                if (relocateFunction.isValidForRelocating(gridMap, gridMapKey)) {

                    gridUtil.clearConnectionPlaceholdersInGrid(grid);

                    var currentGridPoint = gridUtil.toPoint(gridMap[gridMapKey].x, gridMap[gridMapKey].y);

                    relocateFunction.initRelocateValues(gridMapKey, currentGridPoint);

                    var shortestValidDistanceAmongAttempts = Number.MAX_SAFE_INTEGER;
                    var shortestValidDistanceAmongAttemptsPoint = gridUtil.toPoint(-1, -1);

                    for (var attemptToGetCloser = 0; attemptToGetCloser < 10; attemptToGetCloser++) {

                        var bestGridPoint = gridUtil.getFreeGridPointWithShortestDistanceToPointsBalancedPreferred(grid, relocateFunction.getRelocateTargetPoints(), relocateFunction.isPointToBeConsidered, relocateFunction.getPointToBeConsideredParameters());
                        
                        if (bestGridPoint.x > -1 && bestGridPoint.y > -1) {
                            grid[bestGridPoint.x][bestGridPoint.y] = { element: 'connection' };

                            relocateFunction.calculateMetrics(currentGridPoint, bestGridPoint);

                            if (relocateFunction.isBetterOptionThanCurrent()) {

                                gridUtil.moveElementToNewLocation(grid, gridMap, gridMapKey, currentGridPoint, bestGridPoint);

                                var checkForOptimizationThroughElements = gridUtil.getElementsWhereConnectionLinesPassThrough(gridMap, grid, gridStatistics);
                                if (checkForOptimizationThroughElements.length === 0 && relocateFunction.calculatedValue() < shortestValidDistanceAmongAttempts) {
                                    shortestValidDistanceAmongAttempts = relocateFunction.calculatedValue();
                                    shortestValidDistanceAmongAttemptsPoint = gridUtil.toPoint(bestGridPoint.x, bestGridPoint.y);
                                }

                                gridUtil.moveElementToNewLocation(grid, gridMap, gridMapKey, bestGridPoint, currentGridPoint, 'connection');
                            }
                        }
                    }

                    gridUtil.clearConnectionPlaceholdersInGrid(grid);

                    if (shortestValidDistanceAmongAttemptsPoint.x > -1 && shortestValidDistanceAmongAttemptsPoint.y > -1) {

                        gridUtil.moveElementToNewLocation(grid, gridMap, gridMapKey, currentGridPoint, shortestValidDistanceAmongAttemptsPoint);

                        console.log("Relocated " + gridMapKey + " from " + gridUtil.getLogStr(currentGridPoint) + " to " + gridUtil.getLogStr(shortestValidDistanceAmongAttemptsPoint));

                        this.nrRelocations++;
                    }
                }
            }

            console.log("=== End Attempting To " + relocateFunction.getDescription() + " ===");
        }

    }

    com.mkiii.GridRelocateFunctionCloserToCenterPoint = function (grid, gridMap) {
        this.baseGridRelocateFunction = new com.mkiii.GridRelocateFunction(grid, gridMap, this);
        this.grid = grid;
        this.gridMap = gridMap;
        this.centerPoint = gridUtil.toPoint(-1, -1);
        this.currentGridPointDistanceToCenterPoint = Number.MAX_SAFE_INTEGER;
        this.bestGridPointDistanceToCenterPoint = Number.MAX_SAFE_INTEGER;
        this.directionVectorFromCurrentPointToCenterPoint = gridUtil.toPoint(-1, -1);

        this.relocate = function (grid, gridMap, gridStatistics) {
            this.baseGridRelocateFunction.relocate(grid, gridMap, gridStatistics);
        }

        this.getNrRelocations = function() {
            return this.baseGridRelocateFunction.nrRelocations;
        }

        this.getDescription = function () {
            return "Relocate Points Closer To Center Point";
        }

        this.isValidForRelocating = function (gridMap, gridMapKey) {
            return this.gridMap[gridMapKey];
        }

        this.initRelocateValues = function (gridMapKey, currentGridPoint) {
            this.centerPoint = gridUtil.getCenterPoint(this.gridMap);
            this.currentGridPointDistanceToCenterPoint = gridUtil.distance(currentGridPoint, this.centerPoint);
            this.directionVectorFromCurrentPointToCenterPoint = gridUtil.getDirectionVectorLengthOne(currentGridPoint, this.centerPoint);
        }

        this.getRelocateTargetPoints = function () {
            return [this.centerPoint];
        }

        this.isPointToBeConsidered = function(grid, pointsToCheckWith, pointToCheck, pointToBeConsideredParams) {
            var directionVectorCheckPoint = gridUtil.getDirectionVectorLengthOne(pointToCheck, pointsToCheckWith[0]);

            return directionVectorCheckPoint.x === pointToBeConsideredParams.directionVectorFromCurrentPointToCenterPoint.x
                && directionVectorCheckPoint.y === pointToBeConsideredParams.directionVectorFromCurrentPointToCenterPoint.y;
        }

        this.getPointToBeConsideredParameters = function() {
            return {
                directionVectorFromCurrentPointToCenterPoint: this.directionVectorFromCurrentPointToCenterPoint
            }
        }

        this.calculateMetrics = function (currentGridPoint, bestGridPoint) {
            this.bestGridPointDistanceToCenterPoint = gridUtil.distance(this.centerPoint, bestGridPoint);
        }

        this.isBetterOptionThanCurrent = function () {
            return this.bestGridPointDistanceToCenterPoint < this.currentGridPointDistanceToCenterPoint;
        }

        this.calculatedValue = function () {
            return this.bestGridPointDistanceToCenterPoint;
        }

    }

    com.mkiii.GridRelocateFunctionCloserToAllIncomingElements = function (grid, gridMap) {
        this.baseGridRelocateFunction = new com.mkiii.GridRelocateFunction(grid, gridMap, this);
        this.grid = grid;
        this.gridMap = gridMap;
        this.connectedFromGridPoints = [];
        this.bestGridPointDistanceToFromPoint = Number.MAX_SAFE_INTEGER;
        this.currentGridPointDistanceToFromPoint = Number.MAX_SAFE_INTEGER;

        this.relocate = function (grid, gridMap, gridStatistics) {
            this.baseGridRelocateFunction.relocate(grid, gridMap, gridStatistics);
        }

        this.getNrRelocations = function() {
            return this.baseGridRelocateFunction.nrRelocations;
        }

        this.getDescription = function () {
            return "Relocate Points Closer To All Incoming Points";
        }

        this.isValidForRelocating = function (gridMap, gridMapKey) {
            return !gridUtil.isConnectArrayEmpty(this.gridMap[gridMapKey].connectedFrom);
        }

        this.initRelocateValues = function (gridMapKey, currentGridPoint) {
            var connectedFromKeys = this.gridMap[gridMapKey].connectedFrom;
            this.connectedFromGridPoints = [];
            for (var k = 0; k < connectedFromKeys.length; k++) {
                this.connectedFromGridPoints.push({
                    x: this.gridMap[connectedFromKeys[k]].x,
                    y: this.gridMap[connectedFromKeys[k]].y
                });
            }
            this.currentGridPointDistanceToFromPoint = gridUtil.totalDistance(currentGridPoint, this.connectedFromGridPoints);
        }

        this.getRelocateTargetPoints = function () {
            return this.connectedFromGridPoints;
        }

        this.calculateMetrics = function (currentGridPoint, bestGridPoint) {
            this.bestGridPointDistanceToFromPoint = gridUtil.totalDistance(bestGridPoint, this.connectedFromGridPoints);
        }

        this.isBetterOptionThanCurrent = function () {
            return this.bestGridPointDistanceToFromPoint < this.currentGridPointDistanceToFromPoint;
        }

        this.calculatedValue = function () {
            return this.bestGridPointDistanceToFromPoint;
        }

    }

    com.mkiii.GridRelocateFunctionCloserToFirstIncomingElement = function (grid, gridMap) {
        this.baseGridRelocateFunction = new com.mkiii.GridRelocateFunction(grid, gridMap, this);
        this.grid = grid;
        this.gridMap = gridMap;
        this.connectedFromPoint = { x: -1, y: -1 };
        this.connectedFromPointKey = '';
        this.bestGridPointDistanceToFromPoint = Number.MAX_SAFE_INTEGER;
        this.currentGridPointDistanceToFromPoint = Number.MAX_SAFE_INTEGER;

        this.relocate = function (grid, gridMap, gridStatistics) {
            this.baseGridRelocateFunction.relocate(grid, gridMap, gridStatistics);
        }

        this.getNrRelocations = function() {
            return this.baseGridRelocateFunction.nrRelocations;
        }

        this.getDescription = function () {
            return "Relocate Point Closer To First Incoming Point";
        }

        this.isValidForRelocating = function (gridMap, gridMapKey) {
            return !gridUtil.isConnectArrayEmpty(this.gridMap[gridMapKey].connectedFrom);
        }

        this.initRelocateValues = function (gridMapKey, currentGridPoint) {
            this.connectedFromPointKey = this.gridMap[gridMapKey].connectedFrom[0];
            this.connectedFromPoint = gridUtil.toPoint(this.gridMap[this.connectedFromPointKey].x, this.gridMap[this.connectedFromPointKey].y);
            this.currentGridPointDistanceToFromPoint = gridUtil.distance(this.connectedFromPoint, currentGridPoint);
        }

        this.getRelocateTargetPoints = function () {
            return [this.connectedFromPoint];
        }

        this.calculateMetrics = function (currentGridPoint, bestGridPoint) {
            this.bestGridPointDistanceToFromPoint = gridUtil.distance(this.connectedFromPoint, bestGridPoint);
        }

        this.isBetterOptionThanCurrent = function () {
            return this.bestGridPointDistanceToFromPoint < this.currentGridPointDistanceToFromPoint;
        }

        this.calculatedValue = function () {
            return this.bestGridPointDistanceToFromPoint;
        }

    }

    com.mkiii.GridRelocateFunctionCloserToAllOutgoingElementsNoIncomingElements = function (grid, gridMap) {
        this.baseGridRelocateFunction = new com.mkiii.GridRelocateFunction(grid, gridMap, this);
        this.grid = grid;
        this.gridMap = gridMap;
        this.connectedToGridPoints = [];
        this.bestGridPointDistanceToOutgoingPoints = Number.MAX_SAFE_INTEGER;
        this.currentGridPointDistanceToOutgoingPoints = Number.MAX_SAFE_INTEGER;

        this.relocate = function (grid, gridMap, gridStatistics) {
            this.baseGridRelocateFunction.relocate(grid, gridMap, gridStatistics);
        }

        this.getNrRelocations = function() {
            return this.baseGridRelocateFunction.nrRelocations;
        }

        this.getDescription = function () {
            return "Relocate Points Closer To All Outgoing Points When There Are No Incoming Points";
        }

        this.isValidForRelocating = function (gridMap, gridMapKey) {
            return gridUtil.isConnectArrayEmpty(this.gridMap[gridMapKey].connectedFrom)
            && !gridUtil.isConnectArrayEmpty(this.gridMap[gridMapKey].connectedTo);
        }

        this.initRelocateValues = function (gridMapKey, currentGridPoint) {
            var connectedToKeys = this.gridMap[gridMapKey].connectedTo;
            this.connectedToGridPoints = [];
            for (var k = 0; k < connectedToKeys.length; k++) {
                this.connectedToGridPoints.push({
                    x: this.gridMap[connectedToKeys[k]].x,
                    y: this.gridMap[connectedToKeys[k]].y
                });
            }
            this.currentGridPointDistanceToOutgoingPoints = gridUtil.totalDistance(currentGridPoint, this.connectedToGridPoints);
        }

        this.getRelocateTargetPoints = function () {
            return this.connectedToGridPoints;
        }

        this.calculateMetrics = function (currentGridPoint, bestGridPoint) {
            this.bestGridPointDistanceToOutgoingPoints = gridUtil.totalDistance(bestGridPoint, this.connectedToGridPoints);
        }

        this.isBetterOptionThanCurrent = function () {
            return this.bestGridPointDistanceToOutgoingPoints < this.currentGridPointDistanceToOutgoingPoints;
        }

        this.calculatedValue = function () {
            return this.bestGridPointDistanceToOutgoingPoints;
        }

    }

    com.mkiii.GridRelocateFunctionCloserToAllIngoingElementsNoOutgoingElements = function (grid, gridMap) {
        this.baseGridRelocateFunction = new com.mkiii.GridRelocateFunction(grid, gridMap, this);
        this.grid = grid;
        this.gridMap = gridMap;
        this.connectedFromGridPoints = [];
        this.bestGridPointDistanceToIngoingPoints = Number.MAX_SAFE_INTEGER;
        this.currentGridPointDistanceToIngoingPoints = Number.MAX_SAFE_INTEGER;

        this.relocate = function (grid, gridMap, gridStatistics) {
            this.baseGridRelocateFunction.relocate(grid, gridMap, gridStatistics);
        }

        this.getNrRelocations = function() {
            return this.baseGridRelocateFunction.nrRelocations;
        }

        this.getDescription = function () {
            return "Relocate Points Closer To All Ingoing Points When There Are No Outgoing Points";
        }

        this.isValidForRelocating = function (gridMap, gridMapKey) {
            return !gridUtil.isConnectArrayEmpty(this.gridMap[gridMapKey].connectedFrom)
                && gridUtil.isConnectArrayEmpty(this.gridMap[gridMapKey].connectedTo);
        }

        this.initRelocateValues = function (gridMapKey, currentGridPoint) {
            var connectedFromKeys = this.gridMap[gridMapKey].connectedFrom;
            this.connectedFromGridPoints = [];
            for (var k = 0; k < connectedFromKeys.length; k++) {
                this.connectedFromGridPoints.push({
                    x: this.gridMap[connectedFromKeys[k]].x,
                    y: this.gridMap[connectedFromKeys[k]].y
                });
            }
            this.currentGridPointDistanceToIngoingPoints = gridUtil.totalDistance(currentGridPoint, this.connectedFromGridPoints);
        }

        this.getRelocateTargetPoints = function () {
            return this.connectedFromGridPoints;
        }

        this.calculateMetrics = function (currentGridPoint, bestGridPoint) {
            this.bestGridPointDistanceToIngoingPoints = gridUtil.totalDistance(bestGridPoint, this.connectedFromGridPoints);
        }

        this.isBetterOptionThanCurrent = function () {
            return this.bestGridPointDistanceToIngoingPoints < this.currentGridPointDistanceToIngoingPoints;
        }

        this.calculatedValue = function () {
            return this.bestGridPointDistanceToIngoingPoints;
        }

    }

    com.mkiii.GridRelocateFunctionCloserToAllOutgoingElements = function (grid, gridMap) {
        this.baseGridRelocateFunction = new com.mkiii.GridRelocateFunction(grid, gridMap, this);
        this.grid = grid;
        this.gridMap = gridMap;
        this.connectedToGridPoints = [];
        this.bestGridPointDistanceToOutgoingPoints = Number.MAX_SAFE_INTEGER;
        this.currentGridPointDistanceToOutgoingPoints = Number.MAX_SAFE_INTEGER;

        this.relocate = function (grid, gridMap, gridStatistics) {
            this.baseGridRelocateFunction.relocate(grid, gridMap, gridStatistics);
        }

        this.getNrRelocations = function() {
            return this.baseGridRelocateFunction.nrRelocations;
        }

        this.getDescription = function () {
            return "Relocate Points Closer To All Outgoing Points";
        }

        this.isValidForRelocating = function (gridMap, gridMapKey) {
            return !gridUtil.isConnectArrayEmpty(this.gridMap[gridMapKey].connectedTo);
        }

        this.initRelocateValues = function (gridMapKey, currentGridPoint) {
            var connectedToKeys = this.gridMap[gridMapKey].connectedTo;
            this.connectedToGridPoints = [];
            for (var k = 0; k < connectedToKeys.length; k++) {
                this.connectedToGridPoints.push({
                    x: this.gridMap[connectedToKeys[k]].x,
                    y: this.gridMap[connectedToKeys[k]].y
                });
            }
            this.currentGridPointDistanceToOutgoingPoints = gridUtil.totalDistance(currentGridPoint, this.connectedToGridPoints);
        }

        this.getRelocateTargetPoints = function () {
            return this.connectedToGridPoints;
        }

        this.calculateMetrics = function (currentGridPoint, bestGridPoint) {
            this.bestGridPointDistanceToOutgoingPoints = gridUtil.totalDistance(bestGridPoint, this.connectedToGridPoints);
        }

        this.isBetterOptionThanCurrent = function () {
            return this.bestGridPointDistanceToOutgoingPoints < this.currentGridPointDistanceToOutgoingPoints;
        }

        this.calculatedValue = function () {
            return this.bestGridPointDistanceToOutgoingPoints;
        }

    }

    com.mkiii.GridRelocateFunctionCloserToOnlyIncomingElementNoOutgoingElements = function (grid, gridMap) {
        this.baseGridRelocateFunction = new com.mkiii.GridRelocateFunction(grid, gridMap, this);
        this.grid = grid;
        this.gridMap = gridMap;
        this.connectedFromPoint = { x: -1, y: -1 };
        this.connectedFromPointKey = '';
        this.bestGridPointDistanceToFromPoint = Number.MAX_SAFE_INTEGER;
        this.currentGridPointDistanceToFromPoint = Number.MAX_SAFE_INTEGER;

        this.relocate = function (grid, gridMap, gridStatistics) {
            this.baseGridRelocateFunction.relocate(grid, gridMap, gridStatistics);
        }

        this.getNrRelocations = function() {
            return this.baseGridRelocateFunction.nrRelocations;
        }

        this.getDescription = function () {
            return "Relocate Point Closer To Only Incoming Point No Outgoing Points";
        }

        this.isValidForRelocating = function (gridMap, gridMapKey) {
            return !gridUtil.isConnectArrayEmpty(this.gridMap[gridMapKey].connectedFrom)
                && gridUtil.isConnectArrayEmpty(this.gridMap[gridMapKey].connectedTo)
                && gridUtil.getNrConnections(this.gridMap[gridMapKey].connectedFrom) === 1;
        }

        this.initRelocateValues = function (gridMapKey, currentGridPoint) {
            this.connectedFromPointKey = this.gridMap[gridMapKey].connectedFrom[0];
            this.connectedFromPoint = gridUtil.toPoint(this.gridMap[this.connectedFromPointKey].x, this.gridMap[this.connectedFromPointKey].y);
            this.currentGridPointDistanceToFromPoint = gridUtil.distance(this.connectedFromPoint, currentGridPoint);
        }

        this.getRelocateTargetPoints = function () {
            return [this.connectedFromPoint];
        }

        this.calculateMetrics = function (currentGridPoint, bestGridPoint) {
            this.bestGridPointDistanceToFromPoint = gridUtil.distance(this.connectedFromPoint, bestGridPoint);
        }

        this.isBetterOptionThanCurrent = function () {
            return this.bestGridPointDistanceToFromPoint < this.currentGridPointDistanceToFromPoint;
        }

        this.calculatedValue = function () {
            return this.bestGridPointDistanceToFromPoint;
        }

    }

    com.mkiii.GridRelocateFunctionCloserToOnlyOutgoingElementNoIncomingElements = function (grid, gridMap) {
        this.baseGridRelocateFunction = new com.mkiii.GridRelocateFunction(grid, gridMap, this);
        this.grid = grid;
        this.gridMap = gridMap;
        this.connectedToPoint = { x: -1, y: -1 };
        this.connectedToPointKey = '';
        this.bestGridPointDistanceTowardsToPoint = Number.MAX_SAFE_INTEGER;
        this.currentGridPointDistanceTowardsToPoint = Number.MAX_SAFE_INTEGER;

        this.relocate = function (grid, gridMap, gridStatistics) {
            this.baseGridRelocateFunction.relocate(grid, gridMap, gridStatistics);
        }

        this.getNrRelocations = function() {
            return this.baseGridRelocateFunction.nrRelocations;
        }

        this.getDescription = function () {
            return "Relocate Point Closer To Only Outgoing Point No Incoming Points";
        }

        this.isValidForRelocating = function (gridMap, gridMapKey) {
            return gridUtil.isConnectArrayEmpty(this.gridMap[gridMapKey].connectedFrom)
                && !gridUtil.isConnectArrayEmpty(this.gridMap[gridMapKey].connectedTo)
                && gridUtil.getNrConnections(this.gridMap[gridMapKey].connectedTo) === 1;
        }

        this.initRelocateValues = function (gridMapKey, currentGridPoint) {
            this.connectedToPointKey = this.gridMap[gridMapKey].connectedTo[0];
            this.connectedToPoint = gridUtil.toPoint(this.gridMap[this.connectedToPointKey].x, this.gridMap[this.connectedToPointKey].y);
            this.currentGridPointDistanceTowardsToPoint = gridUtil.distance(this.connectedToPoint, currentGridPoint);
        }

        this.getRelocateTargetPoints = function () {
            return [this.connectedToPoint];
        }

        this.calculateMetrics = function (currentGridPoint, bestGridPoint) {
            this.bestGridPointDistanceTowardsToPoint = gridUtil.distance(this.connectedToPoint, bestGridPoint);
        }

        this.isBetterOptionThanCurrent = function () {
            return this.bestGridPointDistanceTowardsToPoint < this.currentGridPointDistanceTowardsToPoint;
        }

        this.calculatedValue = function () {
            return this.bestGridPointDistanceTowardsToPoint;
        }

    }

    com.mkiii.GridRelocateFunctionCloserToNeighbours = function (grid, gridMap) {
        this.baseGridRelocateFunction = new com.mkiii.GridRelocateFunction(grid, gridMap, this);
        this.grid = grid;
        this.gridMap = gridMap;
        this.connectedNeighbours = [];
        this.bestGridPointDistanceToNeighbours = Number.MAX_SAFE_INTEGER;
        this.currentGridPointDistanceToNeighbours = Number.MAX_SAFE_INTEGER;

        this.relocate = function (grid, gridMap, gridStatistics) {
            this.baseGridRelocateFunction.relocate(grid, gridMap, gridStatistics);
        }

        this.getNrRelocations = function() {
            return this.baseGridRelocateFunction.nrRelocations;
        }

        this.getDescription = function () {
            return "Relocate Points Closer To Neighbours";
        }

        this.isValidForRelocating = function (gridMap, gridMapKey) {
            return !gridUtil.isConnectArrayEmpty(this.gridMap[gridMapKey].connectedTo)
            || !gridUtil.isConnectArrayEmpty(this.gridMap[gridMapKey].connectedFrom);
        }

        this.initRelocateValues = function (gridMapKey, currentGridPoint) {
            this.connectedNeighbours = [];

            var connectedToKeys = this.gridMap[gridMapKey].connectedTo;

            if (!gridUtil.isConnectArrayEmpty(this.gridMap[gridMapKey].connectedTo)) {
                for (var k = 0; k < connectedToKeys.length; k++) {
                    this.connectedNeighbours.push({
                        x: this.gridMap[connectedToKeys[k]].x,
                        y: this.gridMap[connectedToKeys[k]].y
                    });
                }
            }

            if (!gridUtil.isConnectArrayEmpty(this.gridMap[gridMapKey].connectedFrom)) {
                var connectedFromKeys = this.gridMap[gridMapKey].connectedFrom;
                for (var k = 0; k < connectedFromKeys.length; k++) {
                    this.connectedNeighbours.push({
                        x: this.gridMap[connectedFromKeys[k]].x,
                        y: this.gridMap[connectedFromKeys[k]].y
                    });
                }
            }

            this.currentGridPointDistanceToNeighbours = gridUtil.totalDistance(currentGridPoint, this.connectedNeighbours);
        }

        this.getRelocateTargetPoints = function () {
            return this.connectedNeighbours;
        }

        this.calculateMetrics = function (currentGridPoint, bestGridPoint) {
            this.bestGridPointDistanceToNeighbours = gridUtil.totalDistance(bestGridPoint, this.connectedNeighbours);
        }

        this.isBetterOptionThanCurrent = function () {
            return this.bestGridPointDistanceToNeighbours < this.currentGridPointDistanceToNeighbours;
        }

        this.calculatedValue = function () {
            return this.bestGridPointDistanceToNeighbours;
        }

    }

    if (typeof module === 'undefined') {
        module = {};
    }

    if (typeof module.exports === 'undefined') {
        module.exports = {};
    }

    if (module && module.exports) {
        module.exports = com.mkiii.GridRelocateFunction;
        module.exports = com.mkiii.GridRelocateFunctionCloserToCenterPoint;
        module.exports = com.mkiii.GridRelocateFunctionCloserToAllIncomingElements;
        module.exports = com.mkiii.GridRelocateFunctionCloserToFirstIncomingElement;
        module.exports = com.mkiii.GridRelocateFunctionCloserToAllOutgoingElementsNoIncomingElements;
        module.exports = com.mkiii.GridRelocateFunctionCloserToAllIngoingElementsNoOutgoingElements;
        module.exports = com.mkiii.GridRelocateFunctionCloserToAllOutgoingElements;
        module.exports = com.mkiii.GridRelocateFunctionCloserToOnlyIncomingElementNoOutgoingElements;
        module.exports = com.mkiii.GridRelocateFunctionCloserToOnlyOutgoingElementNoIncomingElements;
        module.exports = com.mkiii.GridRelocateFunctionCloserToNeighbours;
    }

})();