(function () {

    if (typeof com === 'undefined') {
        com = {};
    }

    if (typeof com.mkiii === 'undefined') {
        com.mkiii = {};
    }

    com.mkiii.OrganizeNodesAndLinks = function(nodes, links) {
        this.nodes = nodes;
        this.links = links;

        this.connectedSets = getConnectedSets();
        this.incomingLinksMap = getIncomingLinksMap();
        this.outgoingLinksMap = getOutgoingLinksMap();

        addLinksToConnectedSets(this.connectedSets, this.incomingLinksMap, this.outgoingLinksMap);
        sortConnectedSets(this.connectedSets);

        function getConnectedSets() {
            var connectedSets = [];

            for (var i = 0; i < links.length; i++) {
                var link = links[i];

                var sourceCheck = isNodeInConnectedSet(connectedSets, link.source);
                var targetCheck = isNodeInConnectedSet(connectedSets, link.target);

                var sourceNode = getNodeFromId(link.source);
                var targetNode = getNodeFromId(link.target);

                if (!sourceCheck.isFound && !targetCheck.isFound) {
                    var newConnectedSet = [];
                    if (sourceNode.id) {
                        newConnectedSet.push(newNodeData(sourceNode));
                    }
                    if (targetNode.id) {
                        newConnectedSet.push(newNodeData(targetNode));
                    }
                    connectedSets.push(newConnectedSet);
                }
                
                if (!sourceCheck.isFound && targetCheck.isFound) {
                    connectedSets[targetCheck.location[0]].push(newNodeData(sourceNode));
                }

                if (sourceCheck.isFound && !targetCheck.isFound) {
                    connectedSets[sourceCheck.location[0]].push(newNodeData(targetNode));
                }
            }

            for (var i = 0; i < nodes.length; i++) {
                var nodeCheck = isNodeInConnectedSet(connectedSets, nodes[i].id);
                if (!nodeCheck.isFound) {
                    var newSet = [];
                    newSet.push(newNodeData(nodes[i]));
                    connectedSets.push(newSet);
                }
            }
            return connectedSets;
        }

        function getNodeFromId(id) {
            for (var i = 0; i < nodes.length; i++) {
                if (nodes[i].id === id) {
                    return nodes[i];
                }
            }
            return {'id': '', 'group': -1};
        }

        function isNodeInConnectedSet(connectedSets, nodeId) {
            for (var s = 0; s < connectedSets.length; s++) {
                for (var t = 0; t < connectedSets[s].length; t++) {
                    if (connectedSets[s][t].node.id === nodeId) {
                        return {
                            location: [s, t],
                            isFound: true
                        };
                    }
                }
            }
            return {
                location: [],
                isFound: false
            };
        }

        function getIncomingLinksMap() {
            var incomingLinksMap = {};
            for (var i = 0; i < links.length; i++) {
                var link = links[i];
                if (!incomingLinksMap[link.target]) {
                    incomingLinksMap[link.target] = [];
                }
                if (incomingLinksMap[link.target].indexOf(link.source) === -1) {
                    incomingLinksMap[link.target].push(link.source);
                }
            }
            return incomingLinksMap;
        }

        function getOutgoingLinksMap() {
            var outgoingLinksMap = {};
            for (var i = 0; i < links.length; i++) {
                var link = links[i];
                if (!outgoingLinksMap[link.source]) {
                    outgoingLinksMap[link.source] = [];
                }
                if (outgoingLinksMap[link.source].indexOf(link.target) === -1) {
                    outgoingLinksMap[link.source].push(link.target);
                }
            }
            return outgoingLinksMap;
        }

        function addLinksToConnectedSets(connectedSets, incomingLinksMap, outgoingLinksMap) {
            for (var i = 0; i < connectedSets.length; i++) {
                for (var j = 0; j < connectedSets[i].length; j++) {
                    var node = connectedSets[i][j].node;
                    connectedSets[i][j].outgoingLinks = [];
                    connectedSets[i][j].incomingLinks = [];

                    if (incomingLinksMap[node.id] && incomingLinksMap[node.id].length > 0) {
                        connectedSets[i][j].incomingLinks = incomingLinksMap[node.id].slice();
                    }

                    if (outgoingLinksMap[node.id] && outgoingLinksMap[node.id].length > 0) {
                        connectedSets[i][j].outgoingLinks = outgoingLinksMap[node.id].slice();
                    }
                }
            }
        }

        function sortConnectedSets(connectedSets) {
            for (var i = 0; i < connectedSets.length; i++) {
                var connectedSet = connectedSets[i];
                connectedSet.sort((i,j) => (j.incomingLinks.length + j.outgoingLinks.length) - (i.incomingLinks.length + i.outgoingLinks.length));
            }
        }

        function newNodeData(node) {
            return {
                node: node,
                hasIncomingLinks: function() {
                    return this.incomingLinks && this.incomingLinks.length > 0;
                },
                hasOutgoingLinks: function() {
                    return this.outgoingLinks && this.outgoingLinks.length > 0;
                },
                isBeginPoint: function() {
                    return !this.hasIncomingLinks() && this.hasOutgoingLinks() && this.outgoingLinks.length === 1;
                },
                isEndPoint: function() {
                    return this.hasIncomingLinks() && this.incomingLinks.length === 1 && !this.hasOutgoingLinks();
                }
            }
        }

        this.getNodePositionMap = function(nodeMatrix) {
            var nodePositionMap = {};
            for (var i = 0; i < nodeMatrix.length; i++) {
                if (nodeMatrix[i]) {
                    for (var j = 0; j < nodeMatrix[i].length; j++) {
                        if (nodeMatrix[i][j]) {
                            nodePositionMap[nodeMatrix[i][j]] = {
                                x: i,
                                y: j
                            };
                        }
                    }
                }
            }
            return nodePositionMap;
        }

        this.getCrossingLinksPositionMatrix = function(nodeSet, incomingLinksMap, nodeMatrix) {
            // console.log("nodeSet");
            // console.log(nodeSet);
            // console.log("incomingLinksMap");
            // console.log(incomingLinksMap);
            // console.log("nodeMatrix");
            // console.log(nodeMatrix);

            var crossingLinksPositionMatrix = [];
            for (var i = 0; i < nodeMatrix.length; i++) {
                crossingLinksPositionMatrix[i] = [];
                for (var j = 0; j < nodeMatrix[i].length; j++) {
                    crossingLinksPositionMatrix[i][j]='';
                }
            }

            var nodePositionMap = this.getNodePositionMap(nodeMatrix);
            // console.log("nodePositionMap");
            // console.log(nodePositionMap);

            for (var i = 0; i < nodeSet.length; i++) {
                var nodeData = nodeSet[i];
                var nodeId = nodeData.node.id;
                var nodeIncomingLinks = incomingLinksMap[nodeId];
                var nodePosition = nodePositionMap[nodeId];
                if (nodeIncomingLinks) {
                    for (var j = 0; j < nodeIncomingLinks.length; j++) {
                        var targetId = nodeIncomingLinks[j];
                        var targetPosition = nodePositionMap[targetId];
                        var crossingLinksPositions = getCrossingLinksPositions(nodePosition, targetPosition);
                        // console.log('crossingLinksPositions');
                        // console.log(crossingLinksPositions);
                        for (var pos = 0; pos < crossingLinksPositions.length; pos++) {
                            crossingLinksPositionMatrix[crossingLinksPositions[pos].x][crossingLinksPositions[pos].y] = 'X';
                        }
                    }
                }
            }
            return crossingLinksPositionMatrix;

            function getCrossingLinksPositions(sourcePosition, targetPosition) {
                // console.log(sourcePosition);
                // console.log(targetPosition);
                var dir = {
                    x: targetPosition.x - sourcePosition.x,
                    y: targetPosition.y - sourcePosition.y
                };
                // console.log("dir.x: "+dir.x);
                // console.log("dir.y: "+dir.y);

                if (Math.abs(dir.x) <= 1 && Math.abs(dir.y) <= 1) {
                    return [];
                }

                var crossingLinksPositions = [];

                if (targetPosition.x === sourcePosition.x) {
                    dir.y = dir.y > 0 ? 1 : -1;
                }
                if (targetPosition.y === sourcePosition.y) {
                    dir.x = dir.x > 0 ? 1 : -1;
                }

                if (Math.abs(dir.x) > 1 || Math.abs(dir.y) > 1) {
                    var divFactor = Math.max(Math.abs(dir.x), Math.abs(dir.y));
                    dir.x = dir.x / divFactor;
                    dir.y = dir.y / divFactor;
                }

                // console.log("Normalized dir.x: "+dir.x);
                // console.log("Normalized dir.y: "+dir.y);

                var nrSteps = 0;
                if (Math.abs(dir.x) > 0 && (Math.abs(dir.x) <= Math.abs(dir.y) || dir.y === 0)) {
                    nrSteps = Math.ceil( (targetPosition.x - sourcePosition.x) / dir.x );
                } else if (Math.abs(dir.y > 0) && (Math.abs(dir.y) <= Math.abs(dir.x) || dir.x === 0)) {
                    nrSteps = Math.ceil( (targetPosition.y - sourcePosition.y) / dir.y );
                }

                // console.log("nrSteps: "+nrSteps);

                for (var step = 1; step < nrSteps; step++) {
                    var crossingPosition = {
                        x: Math.round(sourcePosition.x + step * dir.x),
                        y: Math.round(sourcePosition.y + step * dir.y)
                    };
                    // console.log('crossingPosition');
                    // console.log(crossingPosition);
                    crossingLinksPositions.push(crossingPosition);
                }

                return crossingLinksPositions;
            }
        }

        this.getDebugStringOfMatrix = function(matrix) {
            if (!matrix || !matrix.length || matrix.length === 0) {
                return '';
            }
            var rows = '';
            for (var i = 0; i < matrix.length; i++) {
                var row = '        ';
                if (matrix[i] && matrix[i].length && matrix[i].length > 0) {
                    for (var j = 0; j < matrix[i].length; j++) {
                        row += getStrOfLength(matrix[i][j], 8) + ' ';
                    }
                }
                rows += row;
                if (i < matrix.length - 1) {
                    rows += '\n';
                }
            }
            return rows;

            function getStrOfLength(str, len) {
                var strOfFixedLength = '';
                for (var n = 0; n < len; n++) {
                    if (n < str.length) {
                        strOfFixedLength += str.charAt(n);
                    } else {
                        strOfFixedLength += '_';
                    }
                }
                return strOfFixedLength;
            }
        }
    }

    if (typeof module === 'undefined') {
        module = {};
    }

    if (typeof module.exports === 'undefined') {
        module.exports = {};
    }

    if (module && module.exports) {
        module.exports = com.mkiii.OrganizeNodesAndLinks;
    }

})();
