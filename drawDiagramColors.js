/*
Copyright 2018 Michel Klabbers

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR 
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
OTHER DEALINGS IN THE SOFTWARE.
*/

(function () {

    if (typeof com === 'undefined') {
        com = {};
    }

    if (typeof com.mkiii === 'undefined') {
        com.mkiii = {};
    }

    com.mkiii.DrawDiagramColors = function(canvasName) {
        this.LIGHT_BLUE = 'rgba(168, 222, 255, 0.95)';
        this.PASTEL_BROWN = 'rgba(222, 196, 168, 0.95)';
        this.PASTEL_BROWN_TRANSPARENT = 'rgba(222, 196, 168, 0.15)';
        this.PASTEL_GREY = 'rgba(222, 222, 222, 0.95)';
        this.PASTEL_GREY_TRANSPARENT = 'rgba(222, 222, 222, 0.15)';
        this.PASTEL_GREEN = 'rgba(196, 222, 168, 0.95)';
        this.PASTEL_LIGHT_BLUE = 'rgba(168, 196, 222, 0.95)';
        this.PASTEL_BLUE = 'rgba(132, 164, 253, 0.95)';
        this.PASTEL_YELLOW = 'rgba(253, 253, 150, 0.95)';
        this.PASTEL_RED = 'rgba(253, 150, 150, 0.95)';
        this.PASTEL_ORANGE = 'rgba(253, 196, 150, 0.95)';
        this.GREEN = 'rgba(96, 222, 128, 0.95)';
        this.LIGHT_YELLOW = 'rgba(255, 255, 0, 0.95)';
        this.canvas = document.getElementById(canvasName);
        this.context = this.canvas.getContext('2d');

        this.getLightBlueToPastelBlueGradient = function(p1, p2) {
            var gradient = this.context.createLinearGradient(p1.x, p1.y, p2.x, p2.y);
            gradient.addColorStop(0, this.LIGHT_BLUE);
            gradient.addColorStop(1, this.PASTEL_BLUE);
            return gradient;
        }

        this.getPastelOrangeToPastelRedGradient = function(p1, p2) {
            var gradient = this.context.createLinearGradient(p1.x, p1.y, p2.x, p2.y);
            gradient.addColorStop(0, this.PASTEL_ORANGE);
            gradient.addColorStop(1, this.PASTEL_RED);
            return gradient;
        }

        this.getPastelYellowToPastelOrangeGradient = function(p1, p2) {
            var gradient = this.context.createLinearGradient(p1.x, p1.y, p2.x, p2.y);
            gradient.addColorStop(0, this.PASTEL_YELLOW);
            gradient.addColorStop(1, this.PASTEL_ORANGE);
            return gradient;
        }

        this.getPastelGreenToGreenGradient = function(p1, p2) {
            var gradient = this.context.createLinearGradient(p1.x, p1.y, p2.x, p2.y);
            gradient.addColorStop(0, this.PASTEL_GREEN);
            gradient.addColorStop(1, this.GREEN);
            return gradient;
        }

        this.getPastelBrownToPastelGreyGradient = function(p1, p2) {
            var gradient = this.context.createLinearGradient(p1.x, p1.y, p2.x, p2.y);
            gradient.addColorStop(0, this.PASTEL_BROWN);
            gradient.addColorStop(1, this.PASTEL_GREY);
            return gradient;
        }

        this.getTransparentPastelBrownToPastelGreyGradient = function(p1, p2) {
            var gradient = this.context.createLinearGradient(p1.x, p1.y, p2.x, p2.y);
            gradient.addColorStop(0, this.PASTEL_BROWN_TRANSPARENT);
            gradient.addColorStop(1, this.PASTEL_GREY_TRANSPARENT);
            return gradient;
        }
    }

})();