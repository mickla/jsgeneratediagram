/*
Copyright 2018 Michel Klabbers

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR 
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
OTHER DEALINGS IN THE SOFTWARE.
*/

(function () {

    if (typeof com === 'undefined') {
        com = {};
    }

    if (typeof com.mkiii === 'undefined') {
        com.mkiii = {};
    }

    if (typeof performance === 'undefined') {
        performance = {};
        performance.now = function() {
            // performance.now mock
        }
    }

    com.mkiii.GridUtil = function () {

        this.getElementsWhereConnectionLinesPassThrough = function (gridMap, grid, gridStatistics) {
            if (!gridStatistics) {
                throw Error("gridStatistics parameter is mandatory");
            }

            var howCloseToCenter = 44 / gridStatistics.totalSize;
            if (howCloseToCenter < 0.005) {
                return [];
            }


            var startTime = performance.now();
            var nrConnectionsChecked = 0;

            var checkedConnections = [];

            var allThroughElementKeys = [];

            var gridMapKeys = Object.keys(gridMap);
            for (var i = 0; i < gridMapKeys.length; i++) {

                var element = gridMap[gridMapKeys[i]];

                if (element && element.connectedFrom && element.connectedFrom.length > 0 && element.connectedFrom[0] !== "") {

                    for (var p = 0; p < element.connectedFrom.length; p++) {
                        var previous = element.connectedFrom[p];
                        if (checkedConnections.indexOf(gridMapKeys[i] + "-" + previous) === -1) {
                            var previousElement = gridMap[previous];

                            //console.log(gridMapKeys[i] + "-" + previous);
                            var throughElements = this.getConnectedThroughElements(grid, element, previousElement, howCloseToCenter);
                            if (throughElements && throughElements.length > 0) {
                                allThroughElementKeys.push(throughElements);
                            }

                            checkedConnections.push(gridMapKeys[i] + "-" + previous);
                            checkedConnections.push(previous + "-" + gridMapKeys[i]);
                            nrConnectionsChecked++;
                        }
                    }
                }

                if (element && element.connectedTo && element.connectedTo.length > 0 && element.connectedTo[0] !== "") {

                    for (var n = 0; n < element.connectedTo.length; n++) {
 
                        var next = element.connectedTo[n];
                        if (checkedConnections.indexOf(gridMapKeys[i] + "-" + next) === -1) {
                            var nextElement = gridMap[next];

                            //console.log(gridMapKeys[i] + "-" + next);
                            var throughElements = this.getConnectedThroughElements(grid, element, nextElement, howCloseToCenter);
                            if (throughElements && throughElements.length > 0) {
                                allThroughElementKeys.push(throughElements);
                            }

                            checkedConnections.push(gridMapKeys[i] + "-" + next);
                            checkedConnections.push(next + "-" + gridMapKeys[i]);
                            nrConnectionsChecked++;
                        }
                    }
                }
            }

            allThroughElementKeys = this.flatten(allThroughElementKeys);

            var endTime = performance.now();

            //console.log("" + nrConnectionsChecked + " connections checked, call to getElementsWhereConnectionLinesPassThrough took " + (endTime - startTime) + " milliseconds.")

            return allThroughElementKeys;

        }

        this.getConnectedThroughElements = function (grid, element, otherElement, howCloseToCenter) {
            var throughElements = [];

            var dirX = otherElement.x - element.x;
            var dirY = otherElement.y - element.y;

            if (Math.abs(dirX) < 0.1 && Math.abs(dirY) < 0.1) {
                return throughElements;
            }

            var normalizeFactor = Math.max(Math.abs(dirX), Math.abs(dirY));

            dirX = dirX / normalizeFactor;
            dirY = dirY / normalizeFactor;

            var x = element.x + dirX;
            var y = element.y + dirY;

            var count = 0;
            while ((Math.abs(x - otherElement.x) > 0.95 || Math.abs(y - otherElement.y)) > 0.95 && count < 100) {
                var closestX;
                var closestY;
                if (Math.abs(x - Math.floor(x)) < howCloseToCenter) {
                    closestX = Math.floor(x);
                }
                if (Math.abs(x - Math.ceil(x)) < howCloseToCenter) {
                    closestX = Math.ceil(x);
                }
                if (Math.abs(y - Math.floor(y)) < howCloseToCenter) {
                    closestY = Math.floor(y);
                }
                if (Math.abs(y - Math.ceil(y)) < howCloseToCenter) {
                    closestY = Math.ceil(y);
                }

                if (closestX && closestY && grid[closestX][closestY] && grid[closestX][closestY].element) {
                    var throughElement = grid[closestX][closestY].element;
                    if (throughElement) {
                        if (throughElements.indexOf(throughElement) === -1 && throughElement !== 'connection') {
                            throughElements.push(throughElement);
                            //console.log(throughElement);
                        }
                    }
                }
                y += dirY;
                x += dirX;
                count++;
            }

            return throughElements;
        }

        this.isYDistanceBetweenElementsGreaterThan = function (element, otherElement, distance) {
            return element.x === otherElement.x && Math.abs(element.y - otherElement.y) > distance;
        }

        this.isXDistanceBetweenElementsGreaterThan = function (element, otherElement, distance) {
            return element.y === otherElement.y && Math.abs(element.x - otherElement.x) > distance;
        }

        this.isDiagonalDistanceBetweenElementsGreaterThan = function (element, otherElement, distance) {
            return element.x !== otherElement.x
                && element.y !== otherElement.y
                && Math.abs(element.x - otherElement.x) === Math.abs(element.y - otherElement.y)
                && Math.abs(element.x - otherElement.x) > distance;
        }

        this.toPoint = function (x, y) {
            return {
                x: x,
                y: y
            };
        }

        this.totalDistance = function (element, otherElements) {
            var totalDistance = 0;
            for (var i = 0; i < otherElements.length; i++) {
                totalDistance += this.distance(element, otherElements[i]);
            }
            return totalDistance;
        }

        this.getCenterPoint = function (gridMap) {
            var gridMapKeys = Object.keys(gridMap);
            var totalX = 0;
            var totalY = 0;
            for (var i = 0; i < gridMapKeys.length; i++) {
                var gridMapKey = gridMapKeys[i];
                totalX += gridMap[gridMapKey].x;
                totalY += gridMap[gridMapKey].y;
            }
            return {
                x: totalX / gridMapKeys.length,
                y: totalY / gridMapKeys.length
            };
        }

        this.clearConnectionPlaceholdersInGrid = function (grid) {
            for (var u = 0; u < grid.length; u++) {
                for (var v = 0; v < grid[u].length; v++) {
                    if (grid[u][v] && grid[u][v].element === 'connection') {
                        grid[u][v].element = null;
                    }
                }
            }
        }

        this.distance = function (element, otherElement) {
            return Math.sqrt(Math.pow(element.x - otherElement.x, 2) + Math.pow(element.y - otherElement.y, 2));
        }

        this.distanceDG = function(element, otherElement) {
            return Math.max(Math.abs(element.x - otherElement.x), Math.abs(element.y - otherElement.y));
        }

        this.getDistanceBetweenGridPoints = function (p1, p2) {
            return Math.sqrt(Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2));
        }

        this.getFreeGridPointWithShortestDistanceToPointsBalancedPreferred = function (grid, points, isPointToBeConsidered = function() { return true; }, pointToBeConsideredParameters = {}) {
            var shortestDistance = Number.MAX_SAFE_INTEGER;
            var bestPoint = { x: -1, y: -1 };
            for (var i = 0; i < grid.length; i++) {
                for (var j = 0; j < grid[i].length; j++) {
                    if (!grid[i][j].element && (!isPointToBeConsidered || isPointToBeConsidered(grid, points, this.toPoint(i, j), pointToBeConsideredParameters))) {
                        var distance = this.getDistanceBetweenPointAndPointsBalancedPreferred({ x: i, y: j }, points);
                        if (distance < shortestDistance) {
                            shortestDistance = distance;
                            bestPoint = { x: i, y: j };
                        }
                    }
                }
            }
            return bestPoint;
        }

        this.getDistanceBetweenPointAndPointsBalancedPreferred = function (p1, points) {
            if (!p1 || !points || points.length === 0) {
                return 0;
            }

            var totalDistance = 0;
            var distances = [];
            for (var i = 0; i < points.length; i++) {
                distances[i] = this.getDistanceBetweenGridPoints(p1, points[i]);
                totalDistance += distances[i];
            }

            var avgDistance = totalDistance / points.length;

            var differenceToAvgTotal = 0;
            for (var i = 0; i < points.length; i++) {
                var differenceToAvg = Math.abs(avgDistance - distances[i]);
                differenceToAvgTotal += differenceToAvg;
            }

            return totalDistance + differenceToAvgTotal;
        }

        this.moveElementToNewLocation = function (grid, gridMap, elementKey, oldLocation, newLocation, oldLocationElementKey = null) {
            grid[newLocation.x][newLocation.y] = {
                element: elementKey
            }

            grid[oldLocation.x][oldLocation.y] = {
                element: oldLocationElementKey
            }

            gridMap[elementKey].x = newLocation.x;
            gridMap[elementKey].y = newLocation.y;
        }

        this.isElementNotKnownYetInGrid = function (gridMap, element) {
            return Object.keys(gridMap).indexOf(element) === -1;
        }

        this.isElementKnownInGrid = function (gridMap, element) {
            return Object.keys(gridMap).indexOf(element) !== -1;
        }

        this.flatten = function flatten(arr) {
            return arr.reduce(function (flat, toFlatten) {
                return flat.concat(Array.isArray(toFlatten) ? flatten(toFlatten) : toFlatten);
            }, []);
        }

        this.getLogStr = function (point) {
            return "(" + point.x + ", " + point.y + ")";
        }

        this.isConnectArrayEmpty = function(connectArray) {
            return !connectArray || connectArray.length === 0 || areAllStringValuesEmpty(connectArray);

            function areAllStringValuesEmpty(arr) {
                var allStringValuesEmpty = false;
                for (var i = 0; i < arr.length; i++) {
                    if (!arr[i] || arr[i].trim() === '') {
                        allStringValuesEmpty = true;
                    }
                }
                return allStringValuesEmpty;
            }
        }

        this.getNrConnections = function(connectArray) {
            if (!connectArray || connectArray.length === 0) {
                return 0;
            }
            return nrStringValuesNotEmpty(connectArray);

            function nrStringValuesNotEmpty(arr) {
                var nrStringValuesNotEmpty = 0;
                for (var i = 0; i < arr.length; i++) {
                    if (arr[i] && arr[i].trim() !== '') {
                        nrStringValuesNotEmpty++;
                    }
                }
                return nrStringValuesNotEmpty;
            }
        }

        this.getDirectionVectorLengthOne = function(p1, p2) {
            var xCheckDiff = p2.x - p1.x;
            if (xCheckDiff !== 0) {
                xCheckDiff = xCheckDiff / Math.abs(xCheckDiff);
            }
            var yCheckDiff = p2.y - p1.y;
            if (yCheckDiff !== 0) {
                yCheckDiff = yCheckDiff / Math.abs(yCheckDiff);
            }

            return {
                x: xCheckDiff,
                y: yCheckDiff
            }
        }

        this.getChecksum = function(str) {
            var checksum = 0;
            for (var i = 0; i < str.length; i++) {
                checksum += (str.charCodeAt(i) * (i + 1));
            }
            return checksum;
        }

        this.getLocalStorageString = function(str) {
            return "com.mkiii.grid." + str;
        }
    }

    if (typeof module === 'undefined') {
        module = {};
    }

    if (typeof module.exports === 'undefined') {
        module.exports = {};
    }

    if (module && module.exports) {
        module.exports = com.mkiii.GridUtil;
    }

})();